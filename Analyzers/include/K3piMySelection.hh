#ifndef K3PIMYSELECTION_HH
#define K3PIMYSELECTION_HH

#include "Analyzer.hh"
#include "DownstreamTrack.hh"
#include "L0PrimitiveHandler.hh"
#include "MatchingGTKtoStrawVertex.hh"
#include "SpectrometerTrackVertex.hh"
#include "StrawSegmentAlgorithm.hh"
#include "TwoLinesCDA.hh"
#include "VertexLSF.hh"

#include <TCanvas.h>

#include <stdlib.h>
#include <vector>

class TH1I;
class TH2F;
class TGraph;
class TTree;

class K3piMySelection: public NA62Analysis::Analyzer {
public:
  explicit K3piMySelection(NA62Analysis::Core::BaseAnalysis *ba);
  ~K3piMySelection();
  void InitHist();
  void InitOutput();
  void Process(Int_t);
  void StartOfBurstUser();
  void EndOfBurstUser();
  void StartOfRunUser();
  void EndOfRunUser();
  void EndOfJobUser();
  void PostProcess();
  void DrawPlot(){};

private:
  const Double_t MeanDt = 0.;
  const Double_t SigmaDt = 0.14;
  const Double_t MeanPld = 1.20;
  const Double_t SigmaPld = 0.50;
  const Double_t like_m =
    TMath::Gaus(0., MeanDt, SigmaDt, true) * TMath::Landau(MeanPld, MeanPld, SigmaPld, true);
  std::vector<TString> fcut;
  std::vector<TString> fmask;
  std::unique_ptr<TwoLinesCDA> ftlcda;
  std::unique_ptr<MatchingGTKtoStrawVertex> fMatchGTK;
  NA62Analysis::Core::L0PrimitiveHandler *fPrimitiveHandler;
  Int_t fIDMT;
  Int_t fIDM3;
  Bool_t fIsL0MT;
  Bool_t fIsL0M3;
  Bool_t fIsL1MT;
  Bool_t fIsL1M3;
  Bool_t fIsL0M5MO1;
  Bool_t fIsL0M5LKr;

  // event info
  Int_t fGTKCandIndex;
  Double_t fVertexTime;
  Int_t fVtxIndex;
  Double_t fTriggerTime;
  UInt_t fNSTRAWTracks;
  UInt_t fNVertices;
  Double_t fTotalClusterEnergy;
  Int_t fNClusters;
  Int_t fNInTimeClusters;
  Double_t fTotalNotMatchedClusterEnergy;
  Int_t fInTimeNotMatchedNClusters;
  TVector3 fBeamThreeMomt;
  TVector3 fGTKMomt;
  TVector3 fGTKMomt_smear;  
  Double_t fvpt;
  Double_t fvpt_smear;  
  Double_t fvtxZ;
  Double_t fPosPion1P3;
  Double_t fPosPion2P3;
  Double_t fNegPionP3;
  Int_t fPosPion1TrackID;
  Int_t fPosPion2TrackID;  
  Int_t fNegPionTrackID;
  Int_t fChosen2trVtxIndex;
  Double_t fMMissSq;
  Double_t fInvMass;

  Double_t fPosPion1TotalQuality;
  Double_t fPosPion2TotalQuality;
  Double_t fNegPionTotalQuality;
  Double_t fPosPion1HoughQuality;
  Double_t fPosPion2HoughQuality;
  Double_t fNegPionHoughQuality;
  std::vector<std::vector<Double_t>> fPosPion1XY;
  std::vector<std::vector<Double_t>> fPosPion2XY;
  std::vector<std::vector<Double_t>> fNegPionXY;
  Double_t fDecayedPionXYch0[2];
  Double_t fDecayedPionXYch1[2];
  Double_t fNuP3_RF;
  Double_t fcost;
  Double_t fThetaPosPiDecayedPi;
  Double_t fThetaNegPiDecayedPi;
  std::array<Int_t, 3> fTrackID;
  std::array<Int_t, 3> fTrackCharge;
  std::array<Double_t, 3> fTrackTime;
  std::array<TVector3, 3> fTrackThreeMomt;

  // outputs
  Bool_t fEventSelected;
  Int_t
  fChosenVtxIndex;  ///< Index of the used 3-track vertex in the SpectrometerVertexBuilder.Output3 array
  Double_t fChosenVertexTime;
  Double_t fChosenKTAGTime;
  std::array<Int_t, 3> fChosenTrackID;
  std::array<Double_t, 3> fChosenTrackTime;
  TLorentzVector fTrueKaonP;
  TLorentzVector fKaonP;
  TLorentzVector fPosPion1P;
  TLorentzVector fPosPion2P;
  TLorentzVector fNegPionP;

  // cut parameters
  Double_t fCutTrackLowMomt;
  Double_t fCutTrackHighMomt;
  Double_t fCutTrackChi2;
  Double_t fCutTrackAfterBeforeFitMomDiff;
  Double_t fCutVertexChi2;
  Double_t fCutZVertexMin;
  Double_t fCutZVertexMax;
  Double_t fCutStraw1Separation;
  Double_t fCutLKrSeparation;
  Double_t fCutVertexTriggerTime;
  Double_t fCutVertexKTAGTime;
  Double_t fCutTriggerKTAGTime;
  Double_t fCutTrackVertexTime;
  Double_t fCutEoPMaximal;
  Double_t fCutMuonVertexMUV3Time;
  Double_t fCutPionVertexMUV3Time;
  Double_t fCutPionLKrEoP;
  Double_t fCutMuonLKrEoP;  
  Double_t fCutLKrClusterDDeadCell;
  Double_t fCutdGTKMomt;
  Double_t fCutEllipseY;
  Double_t fCutEllipseA;
  Double_t fCutEllipseB;
  Double_t fCutTotalQuality;
  Double_t fCutM3Pi;

  // Vertexing tool and Downstream track builder outputs
  std::vector<SpectrometerTrackVertex> fVertices;
  std::vector<SpectrometerTrackVertex> fVertices2tr;
  std::vector<DownstreamTrack> fDownTrack;

  // user functions
  Bool_t TriggerM5Condition();
  Bool_t TriggerM3Condition();
  Double_t getLikelihood(Double_t, Double_t);
  Bool_t tracksPassCuts();
  Bool_t isMuon(UInt_t);
  Bool_t isPion(UInt_t);
  TVector3 GTKSmearing(TVector3);
  Bool_t ProcessEvent();
  Double_t KTAGTime();
  void FillHistos(TString, TString);
  void CutApply(TString);
};
#endif
