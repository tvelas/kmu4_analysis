#ifndef KMU4STRICTSELECTION_HH
#define KMU4STRICTSELECTION_HH

#include "Analyzer.hh"
#include "DownstreamTrack.hh"
#include "L0PrimitiveHandler.hh"
#include "MatchingGTKtoStrawVertex.hh"
#include "SpectrometerTrackVertex.hh"
#include "StrawSegmentAlgorithm.hh"
#include "TwoLinesCDA.hh"
#include "VertexLSF.hh"

#include <TCanvas.h>

#include <stdlib.h>
#include <vector>

class TH1I;
class TH2F;
class TGraph;
class TTree;

class Kmu4StrictSelection: public NA62Analysis::Analyzer {
public:
  explicit Kmu4StrictSelection(NA62Analysis::Core::BaseAnalysis *ba);
  ~Kmu4StrictSelection();
  void InitHist();
  void InitOutput();
  void Process(Int_t);
  void StartOfBurstUser();
  void EndOfBurstUser();
  void StartOfRunUser();
  void EndOfRunUser();
  void EndOfJobUser();
  void PostProcess();
  void DrawPlot(){};

private:
  const Double_t MeanDt = 0.;
  const Double_t SigmaDt = 0.14;
  const Double_t MeanPld = 1.20;
  const Double_t SigmaPld = 0.50;
  const Double_t like_m =
    TMath::Gaus(0., MeanDt, SigmaDt, true) * TMath::Landau(MeanPld, MeanPld, SigmaPld, true);
  std::vector<std::vector<TString>> fcut;
  TString fPIDSwap;
  std::vector<TString> fmask;
  std::vector<TString> fsr;
  std::unique_ptr<PointLineDistance> fPLD;
  std::unique_ptr<TwoLinesCDA> ftlcda;
  std::unique_ptr<MatchingGTKtoStrawVertex> fMatchGTK;
  NA62Analysis::Core::L0PrimitiveHandler *fPrimitiveHandler;
  VertexLSF fVertexLSF;
  Int_t fIDM5;
  Int_t fIDM3;
  Bool_t fIsL0M5;
  Bool_t fIsL0M3;
  Bool_t fIsL1M5;
  Bool_t fIsL1M3;
  Bool_t fEmulatedLKr10;

  // event info
  Int_t fGTKCandIndex;
  Double_t fVertexTime;
  std::unique_ptr<TH3D> hdGTK;
  std::unique_ptr<TGraphAsymmErrors> gProbLKr10;
  std::unique_ptr<TH2D> hLKrPosPionClustersEnergyVsMomentum;
  std::unique_ptr<TH2D> hLKrNegPionClustersEnergyVsMomentum;
  std::unique_ptr<TH2D> hLKrPosMuonClustersEnergyVsMomentum;  
  Int_t fVtxIndex;
  Double_t fTriggerTime;
  UInt_t fNSTRAWTracks;
  UInt_t fNVertices;
  Double_t fTotalClusterEnergy;
  Int_t fNClusters;
  Int_t fNInTimeClusters;
  Double_t fTotalNotMatchedClusterEnergy;
  Int_t fInTimeNotMatchedNClusters;
  TVector3 fBeamThreeMomt;
  TVector3 fGTKMomt;
  Double_t fmomt;
  Double_t fbeam;
  Double_t fvpt;
  Double_t fvtxZ;
  Double_t fPosMuonP3;
  Double_t fPosPionP3;
  Double_t fNegPionP3;
  Int_t fPosPionTrackID;
  Int_t fNegPionTrackID;
  Int_t fPosMuonTrackID;  
  Int_t fChosen2trVtxIndex;
  Double_t fPLDMuonPair;
  Double_t fMMissSq;
  Double_t fMMissSq_nosmear;
  Double_t fMMissSq_2pi;
  Double_t fMMissSq_mu;
  Double_t fMMissSq_fmID;
  Double_t fPosPiTotalQuality;
  Double_t fNegPiTotalQuality;
  Double_t fPosMuTotalQuality;
  Double_t fPosPiHoughQuality;
  Double_t fNegPiHoughQuality;
  Double_t fPosMuHoughQuality;
  std::vector<std::vector<Double_t>> fPosPiXY;
  std::vector<std::vector<Double_t>> fNegPiXY;
  std::vector<std::vector<Double_t>> fPosMuXY;
  Double_t fPLDMuonGTK2Pi;
  Double_t fDecayedPionXYch0[2];
  Double_t fDecayedPionXYch1[2];
  Double_t fPosMudSlope;
  Double_t fMuonPionRF3P_mag;
  Double_t fNumomt;
  Double_t fCDAGTKMuon;
  Double_t fPosMuonP3_RF;
  Double_t fcost;
  Double_t fThetaPosPiPosMu;
  Double_t fThetaNegPiPosMu;
  Double_t fThetaPosMuNu;
  Double_t fThetaPosPiNu;
  Double_t fThetaNegPiNu;
  Double_t fThetaPosMuDecayedPi;
  Double_t fThetaPosPiDecayedPi;
  Double_t fThetaNegPiDecayedPi;
  Double_t fWeight;
  Double_t fdGTK_w;
  Double_t fPosPionEnergy;
  Double_t fNegPionEnergy;
  Double_t fPosMuonEnergy;
  std::array<Int_t, 3> fTrackID;
  std::array<Int_t, 3> fTrackCharge;
  std::array<Double_t, 3> fTrackTime;
  std::array<TVector3, 3> fTrackThreeMomt;

  // outputs
  Bool_t fEventSelected;
  Int_t
  fChosenVtxIndex;  ///< Index of the used 3-track vertex in the SpectrometerVertexBuilder.Output3 array
  Double_t fChosenVertexTime;
  Double_t fChosenKTAGTime;
  std::array<Int_t, 3> fChosenTrackID;
  std::array<Double_t, 3> fChosenTrackTime;
  TVector3 fChosenBeamThreeMomt;
  TLorentzVector fTrueKaonP;
  TLorentzVector fKaonP;
  TLorentzVector fKaonP_nosmear;
  TLorentzVector fPosPionP;
  TLorentzVector fNegPionP;
  TLorentzVector fPosMuonP;
  TLorentzVector fPosMuonP_fmID;
  DownstreamTrack fChosenMuonTrack;

  // cut parameters
  Double_t fCutTrackLowMomt;
  Double_t fCutTrackHighMomt;
  Double_t fCutTrackChi2;
  Double_t fCutTrackAfterBeforeFitMomDiff;
  Double_t fCutVertexChi2;
  Double_t fCutZVertexMin;
  Double_t fCutZVertexMax;
  Double_t fCutTotalVertexMomtWrtBeam;
  Double_t fCutVertexPt;
  Double_t fCutStraw1Separation;
  Double_t fCutLKrSeparation;
  Double_t fCutVertexTriggerTime;
  Double_t fCutVertexKTAGTime;
  Double_t fCutTriggerKTAGTime;
  Double_t fCutTrackVertexTime;
  Double_t fCutMuonVertexMUV3Time;
  Double_t fCutPionVertexMUV3Time;
  Double_t fCutMuonLKrEoP;
  Double_t fCutPionLKrEoP;  
  Double_t fCutLKrClusterDDeadCell;

  Double_t fCutdGTKMomt;
  Double_t fCutEllipseY;
  Double_t fCutEllipseA;
  Double_t fCutEllipseB;
  Double_t fCutPosMuTotalQuality;
  Double_t fCutRSmu;
  Double_t fCutL1Smu;
  Double_t fCutL2Smu;
  Double_t fCutCost;
  Double_t fCutMM2PiPiMu_misID;

  Double_t fWeightPosPionEoP;
  Double_t fWeightNegPionEoP;
  Double_t fWeightPosMuonEoP;

  // Vertexing tool and Downstream track builder outputs
  std::vector<SpectrometerTrackVertex> fVertices;
  std::vector<SpectrometerTrackVertex> fVertices2tr;
  std::vector<DownstreamTrack> fDownTrack;

  // user functions
  Bool_t TriggerM5Condition();
  Bool_t TriggerM3Condition();

  Bool_t tracksPassCuts();
  Bool_t ProcessEvent();
  Double_t KTAGTime();
  Bool_t isMuon(UInt_t);
  Bool_t isPion(UInt_t);
  void GTKSmearing(TVector3&);
  Bool_t MatchedTracksTwoTracksVtx(SpectrometerTrackVertex);
  void TwoTracksVtxHisto(TString, TString);
  void TrueFillHistos(TString, TString);
  void FillHistos(TString, TString);
  void CutApply(TString);
};
#endif
