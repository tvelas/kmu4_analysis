#include "K3piMySelection.hh"

#include "BeamParameters.hh"
#include "Event.hh"
#include "GeometricAcceptance.hh"
#include "KaonDecayConstants.hh"
#include "L0PrimitiveHandler.hh"
#include "MatchingGTKtoStrawVertex.hh"
#include "NA62ConditionsService.hh"
#include "Persistency.hh"
#include "TriggerConditions.hh"
#include "VertexLSF.hh"
#include "functions.hh"

#include <TMath.h>

#include <iostream>
#include <stdlib.h>

using namespace NA62Analysis;
using namespace TMath;

K3piMySelection::K3piMySelection(Core::BaseAnalysis *ba):
    Analyzer(ba, "K3piMySelection"), ftlcda(std::make_unique<TwoLinesCDA>()),
    fPrimitiveHandler(GetL0PrimitiveHandler()), fVertexTime(-99.) {

  RequestL0Data();
  RequestL1Data();
  RequestHLTData();
  RequestBeamSpecialTrigger();
  RequestBeamData();
  RequestL0SpecialTrigger();

  RequestTree(new TRecoSpectrometerEvent, "Reco");
  RequestTree(new TRecoGigaTrackerEvent, "Reco");
  RequestTree(new TRecoCedarEvent, "Reco");
  RequestTree(new TRecoLKrEvent, "Reco");
  RequestTree(new TRecoCHODEvent, "Reco");
  RequestTree(new TRecoNewCHODEvent, "Reco");

  // all cuts as parameters
  AddParam("CutTrackLowMomt", &fCutTrackLowMomt, 10000.);
  AddParam("CutTrackHighMomt", &fCutTrackHighMomt, 50000.);
  AddParam("CutTrackChi2", &fCutTrackChi2, 100.);
  AddParam("CutTrackAfterBeforeFitMomDiff", &fCutTrackAfterBeforeFitMomDiff, 20000.);
  AddParam("CutVertexChi2", &fCutVertexChi2, 20.);
  AddParam("CutZVertexMin", &fCutZVertexMin, 110000.);
  AddParam("CutZVertexMax", &fCutZVertexMax, 180000.);
  AddParam("CutStraw1Separation", &fCutStraw1Separation, 15.0);
  AddParam("CutLKrSeparation", &fCutLKrSeparation, 200.0);
  AddParam("CutVertexTriggerTime", &fCutVertexTriggerTime, 5.);
  AddParam("CutVertexKTAGTime", &fCutVertexKTAGTime, 5.);
  AddParam("CutTriggerKTAGTime", &fCutTriggerKTAGTime, 5.);
  AddParam("CutTrackVertexTime", &fCutTrackVertexTime, 5.);
  AddParam("CutEoPMaximal", &fCutEoPMaximal, 0.9);
  AddParam("CutVertexMUV3Time", &fCutMuonVertexMUV3Time, 5.);
  AddParam("CutVertexMUV3Time", &fCutPionVertexMUV3Time, 10.);
  AddParam("CutMuonLKrEoP", &fCutMuonLKrEoP, 0.2);
  AddParam("CutPionLKrEoP", &fCutPionLKrEoP, 0.9);  
  AddParam("CutLKrClusterDistDeadCell", &fCutLKrClusterDDeadCell, 20);

  // Cuts on background
  AddParam("CutdGTKMomentum", &fCutdGTKMomt, 3.);
  AddParam("CutEllipseY", &fCutEllipseY, 0.01275);
  AddParam("CutEllipseA", &fCutEllipseA, 0.01235);
  AddParam("CutEllipseB", &fCutEllipseB, 0.0136167);
  AddParam("CutTotalQuality", &fCutTotalQuality, 1.49);
  AddParam("CutM3Pi", &fCutM3Pi, 0.005);

  fMatchGTK = std::make_unique<MatchingGTKtoStrawVertex>(ba, this, "MatchGTK");
  fMatchGTK->SetGTKMatchingAlgorithm(MatchingGTKtoStrawVertex::kChi2); // or kChi2
  //fMatchGTK->SetGTKMatchingAlgorithm(MatchingGTKtoStrawVertex::kLikelihood);
  //fMatchGTK->SetGTKMatchingMinLikelihood(0.01); 
  fMatchGTK->SetGTKMatchingMaxDtWithVertex(1.);    // ns
  // Choose the appropriate GTK matching quality cut from the two below
  fMatchGTK->SetGTKMatchingMaxChi2(20.);           // sensible interval may be (10, 50)
}

K3piMySelection::~K3piMySelection() {
}

void K3piMySelection::InitOutput() {
  // registering outputs
  RegisterOutput("EventSelected", &fEventSelected);
  RegisterOutput("VertexIndex", &fChosenVtxIndex);
  RegisterOutput("VertexTime", &fChosenVertexTime);
}

void K3piMySelection::InitHist() {
  BookHisto(new TH1F("hNRuns", "Number of runs", 1, 0, 1));
  BookHisto(new TH1F("hNBursts", "Number of bursts", 1, 0, 1));
  BookHisto(new TH1F("hVerticesSize", "Number of vertices per event", 5, 0, 5));
  BookHisto(new TH1F("hTrueVtxZ", "True vertex Z; z [mm]", 300, 0., 300000.));
  BookHisto(new TH1F("hNTracks", "Number of reconstructed tracks per event", 20, -0.5, 19.5));
  BookHisto(new TH1F("hVtxCharge", "Vertex charge", 11, -5.5, 5.5));
  BookHisto(new TH1F("hVtxChi2", "Vertex chi2", 100, 0., 100.));
  BookHisto(new TH1F("hGTKMomtX", "p_{x,GTK}; p [MeV/c]", 300, -10., 190.));
  BookHisto(new TH1F("hGTKMomtY", "p_{y,GTK}; p [MeV/c]", 300, -110., 110.));
  BookHisto(new TH1F("hGTKMomtZ", "p_{z,GTK}; p [MeV/c]", 300, 72000., 78000.));
  BookHisto(new TH1F("h3PiMomtX", "p_{x,3#pi}; p [MeV/c]", 300, -10., 190.));
  BookHisto(new TH1F("h3PiMomtY", "p_{y,3#pi}; p [MeV/c]", 300, -110., 110.));
  BookHisto(new TH1F("h3PiMomtZ", "p_{z,3#pi}; p [MeV/c]", 300, 72000., 78000.));
  BookHisto(new TH1F("hGTK3PiMomtX", "p_{x,GTK}-p_{x,3#pi}; p [MeV/c]", 300, -30., 30.));
  BookHisto(new TH1F("hGTK3PiMomtY", "p_{y,GTK}-p_{y,3#pi}; p [MeV/c]", 300, -30., 30.));
  BookHisto(new TH1F("hGTK3PiMomtZ", "p_{z,GTK}-p_{z,3#pi}; p [MeV/c]", 300, -2000., 2000.));
  BookHisto(new TH1F("hGTK3PiMomtZabs", "|p_{z,GTK}-p_{z,3#pi}|; p [MeV/c]", 300, 0., 5000.));
  BookHisto(new TH1F("hVtxPt", "Vertex pt; p [MeV/c]", 300, 0., 100.));    
  BookHisto(new TH1F("hGTK3PiMomtX_smear", "p_{x,GTK}-p_{x,3#pi}; p [MeV/c]", 300, -30., 30.));
  BookHisto(new TH1F("hGTK3PiMomtY_smear", "p_{y,GTK}-p_{y,3#pi}; p [MeV/c]", 300, -30., 30.));
  BookHisto(new TH1F("hGTK3PiMomtZ_smear", "p_{z,GTK}-p_{z,3#pi}; p [MeV/c]", 300, -2000., 2000.));
  BookHisto(new TH1F("hGTK3PiMomtZabs_smear", "|p_{z,GTK}-p_{z,3#pi}|; p [MeV/c]", 300, 0., 5000.));
  BookHisto(new TH1F("hVtxPt_smear", "Vertex pt; p [MeV/c]", 300, 0., 100.));  
  BookHisto(new TH1F("hDistanceStraw1", "Distance between track pairs in Straw1;[mm]",
    500, 0, 1000));
  BookHisto(new TH1F("hDistanceLKr", "Distance between track pairs in LKr;[mm]", 500, 0, 2000));
  BookHisto(new TH1F("hNStrawHitsInCandidate", "Number of hits in candidate", 50, 0, 50));
  BookHisto(new TH1F("hNStrawHitsIn4CHCandidate", "Number of hits in 4CH candidate", 50, 0, 50));
  BookHisto(new TH1F("hLKrDDeadCell",
    "Track distance to nearest dead cell;Distance to deal cell [mm]",
    150, 0., 3000.));
  BookHisto(new TH1F("hVtxZ", "Vertex Z; z [mm]", 200, 100000., 200000.));
  BookHisto(new TH1F("hEoP", "Track E/p", 130, 0., 1.3));
  BookHisto(new TH1F("hTrackChi2", "Track chi2", 50, 0., 500.));
  BookHisto(new TH1F("hTrackMomt", "Track p", 80, 0., 80000.));
  BookHisto(new TH1F("hNtracksInTimeWithVtx", "Number of tracks in-time with vertex",
    4, -0.5, 3.5));
  BookHisto(new TH1F("hTrackVertexTime", "Track - vertex time difference", 200, -20., 20.));
  BookHisto(new TH1F("hVertexTriggerTime", "Vertex - trigger time difference", 200, -20., 20.));
  BookHisto(new TH1F("hTriggerKTAGTime", "Trigger - KTAG time difference", 200, -20., 20.));
  BookHisto(new TH1F("hVertexKTAGTime", "Vertex - KTAG time difference", 200, -20., 20.));
  BookHisto(new TH1F("hNVertices", "Number of good 3-track vertices", 10, 0, 10));
  BookHisto(new TH1F("hMask5MO1", "Number of runs", 1, 0, 1));
  BookHisto(new TH1F("hMask5LKr", "Number of runs", 1, 0, 1));
  BookHisto(new TH1F("hChosenVtxChi2", "Chosen Vertex chi2", 100, 0., 100.));

  fmask = {"mask5", "mask3", "mask3ORmask5"};
  fcut = {"/Original", "/AfterdMomtGTK", "/AfterEllipse", "/AfterQuality", "/AfterInvMass"};

  for(UInt_t k = 0; k < fmask.size(); k++) {
    for(UInt_t i = 0; i < fcut.size(); i++) {
      BookHisto(new TH1F(fmask[k] + fcut[i] + "/hdGTKMomt", "|#vec{p}_{K^{+},GTK} - "
	"#vec{p}_{beam}|; p [GeV/c]", 300, 0., 15.));
      BookHisto(new TH1F(fmask[k] + fcut[i] + "/hPosPion1TotalQuality", "Max track total quality",
	300, 0., 5.));
      BookHisto(new TH1F(fmask[k] + fcut[i] + "/hPosPion2TotalQuality", "Max track total quality",
	300, 0., 5.));
      BookHisto(new TH1F(fmask[k] + fcut[i] + "/hNegPionTotalQuality", "Max track total quality",
	300, 0., 5.));

      BookHisto(new TH1F(fmask[k] + fcut[i] + "/hNLKrClusters",
	"Number of LKr clusters;Number of clusters", 10, -0.5, 9.5));
      BookHisto(new TH1F(fmask[k] + fcut[i] + "/hNInTimeLKrClusters",
	"Number of LKr clusters;Number of clusters", 10, -0.5, 9.5));
      BookHisto(new TH2F(fmask[k] + fcut[i] + "/hTotalClusterEnergyVsNInTimeLKrClusters",
	"Total energy of in time clusters vs no of such clusters", 10, -0.5, 9.5, 200, 0., 100.));
      BookHisto(new TH2F(fmask[k] + fcut[i] + "/hNotMatchedClustersEnergyVsNotMatchedLKrClusters",
	"Total energy of not matched in time clusters vs no of such clusters", 10, -0.5, 9.5,
	200, 0., 100.));
      BookHisto(new TH2F(fmask[k] + fcut[i] + "/hVtxMomtVsPt",
	"|#vec{p_{vtx}} - #vec{p_{GTK}}| vs pt; p [GeV/c];p [GeV/c]", 300, 0., 100. / 1e3,
	300, 0., 50.));
      BookHisto(new TH1F(fmask[k] + fcut[i] + "/hChosenPosPion1Momt",
	"Chosen Positive Pion p; p [GeV/c]", 80, 0., 80.));
      BookHisto(new TH1F(fmask[k] + fcut[i] + "/hChosenPosPion2Momt",
	"Chosen Positive Pion p; p [GeV/c]", 80, 0., 80.));
      BookHisto(new TH1F(fmask[k] + fcut[i] + "/hChosenNegPionMomt",
	"Chosen Negative Pion p; p [GeV/c]", 80, 0., 80.));

      BookHisto(new TH1F(fmask[k] + fcut[i] + "/hChosenVtxPt",
	"Chosen Vertex pt; p [GeV/c]", 300, 0., 100. / 1e3));
      BookHisto(new TH1F(fmask[k] + fcut[i] + "/hChosenVtxMomt",
	"Chosen |#vec{p_{vtx}} - #vec{p_{GTK}}|; [GeV/c]", 300, 0., 20.));
      BookHisto(new TH1F(fmask[k] + fcut[i] + "/hChosenVtxZ",
	 "Chosen Vertex Z; z [mm]", 200, 100000., 200000.));
      BookHisto(new TH2F(fmask[k] + fcut[i] + "/hChosenMM2PiPiMuVsVtxZ", 
	"Chosen Vertex (K^{+}-#pi^{+}-#pi^{-}-#mu^{+})^{2} vs Vertex Z;"
	"z [mm]; [GeV^{2}/c^{4}]", 300, 110000, 180000, 300, -0.05, 0.05));
      BookHisto(new TH1F(fmask[k] + fcut[i] + "/hInvMass", 
	"Chosen Vertex m_{3#pi} - m_{K^{+}};[GeV/c^{2}]", 300, -0.05, 0.05));
      BookHisto(new TH1F(fmask[k] + fcut[i] + "/hMM2PiPiPi", "(p_{K}-p_{#pi^{+}}-p_{#pi^{+}}-p_{#pi^{-}})^{2};"
	"[GeV^{2}/c^{4}]", 300, -0.001, 0.001));
      BookHisto(new TH2F(fmask[k] + fcut[i] + "/hChosenVtxPtVs3PiKaon", 
        "Pt vs m_{3#pi}-m_{K^{+}};m_{3#pi} - m_{K^{+}} [GeV/c^{2}];P_{t} [GeV/c]", 300, -0.07, 0.07, 300, 0., 100. / 1e3));

      BookHisto(new TH1F(fmask[k] + fcut[i] + "/hGTKMomt", "|#vec{p}_{K^{+},GTK}|; p [GeV/c]",
	300, 50., 100.));

      BookHisto(new TH1F(fmask[k] + fcut[i] + "/hPosPion1HoughQuality", "Max track Hough quality",
	300, 0., 10.));
      BookHisto(new TH1F(fmask[k] + fcut[i] + "/hPosPion2HoughQuality", "Max track Hough quality",
	300, 0., 10.));
      BookHisto(new TH1F(fmask[k] + fcut[i] + "/hNegPionHoughQuality", "Max track Hough quality",
	300, 0., 10.));
    }
  }

  BookHisto(new TH1F("hChosenPosPion1EoP", "Chosen Positive Pion Track E/p", 130, 0., 1.3));
  BookHisto(new TH1F("hChosenPosPion2EoP", "Chosen Negative Pion Track E/p", 130, 0., 1.3));
  BookHisto(new TH1F("hChosenNegPionEoP", "Chosen Positive Muon Track E/p", 130, 0., 1.3));
  BookHisto(new TH2F("hChosenXYPosPion1ZMUV3",
    "Chosen Positive Pion XY position in MUV3 front plane; X [mm]; Y [mm]", 400, -2000., 2000.,
		                                                            400, -2000., 2000));
  BookHisto(new TH2F("hChosenXYPosPion2ZMUV3",
    "Chosen Positive Pion XY position in MUV3 front plane; X [mm]; Y [mm]", 400, -2000., 2000.,
		                                                            400, -2000., 2000));
  BookHisto(new TH2F("hChosenXYNegPionZMUV3",
    "Chosen Negative Pion XY position in MUV3 front plane; X [mm]; Y [mm]", 400, -2000., 2000.,
		                                                            400, -2000., 2000));
  BookHisto(new TH2F("hChosenXYPosPion1ZLKr",
    "Chosen Positive Pion XY position in LKr front plane; X [mm]; Y [mm]", 400, -2000., 2000.,
		                                                           400, -2000., 2000));
  BookHisto(new TH2F("hChosenXYPosPion2ZLKr",
    "Chosen Positive Pion XY position in LKr front plane; X [mm]; Y [mm]", 400, -2000., 2000.,
		                                                           400, -2000., 2000));
  BookHisto(new TH2F("hChosenXYNegPionZLKr",
    "Chosen Negative Pion XY position in LKr front plane; X [mm]; Y [mm]", 400, -2000., 2000.,
		                                                           400, -2000., 2000));
}

void K3piMySelection::StartOfRunUser() {
  if(!GetIsTree())
    return;
  if(!GetWithEventHeader())
    return;
  FillHisto("hNRuns", 0.5);
  fIDMT = TriggerConditions::GetInstance()->GetL0TriggerID("RICH-QX");
  fIDM3 = TriggerConditions::GetInstance()->GetL0TriggerID("RICH-QX-MO1-LKr10");
  //std::cout << "Vstupnyfile" << GetCurrentFile()->GetName() << '\n';
}

void K3piMySelection::StartOfBurstUser() {
  if(!GetIsTree())
    return;
  if(!GetWithEventHeader())
    return;
  FillHisto("hNBursts", 0.5);
}

void K3piMySelection::Process(Int_t) {
  if(!GetWithEventHeader())
    return;

  if(GetWithMC()) {
    Bool_t EmulatedRICH(false), EmulatedQX(false), EmulatedMO1(false), EmulatedLKr10(false);
    Bool_t EmulatedKTAG_HLT(false);
    Bool_t EmulatedSTRAW_HLT_M3(false), EmulatedSTRAW_HLT_M5(false);

    Int_t RICHtime = fPrimitiveHandler->GetTriggerTime(NA62::Trigger::kL0RICH);
    // x- run number 7000-9600 fill with nof selected km4 events
    EmulatedRICH = fPrimitiveHandler->CheckPrimitives("RICH", RICHtime);
    EmulatedQX   = fPrimitiveHandler->CheckPrimitives("QX", RICHtime);
    EmulatedMO1  = fPrimitiveHandler->CheckPrimitives("MO1", RICHtime);
    EmulatedLKr10  = fPrimitiveHandler->CheckPrimitives("E10", RICHtime);

    HLTEvent *HLTevent = GetHLTData();

    Bool_t KTAGok  = HLTevent->IsL1KTAGProcessed() && !HLTevent->IsL1KTAGEmptyPacket() &&
      !HLTevent->IsL1KTAGBadData();
    Bool_t STRAWok = HLTevent->IsL1StrawProcessed() && !HLTevent->IsL1StrawEmptyPacket() &&
      !HLTevent->IsL1StrawBadData();

    //EmulatedSTRAW_HLT_M3 = ((HLTevent->GetSTRAWResponse() & 0x3) >> 2) == 1;
    //EmulatedSTRAW_HLT_M5 = ((HLTevent->GetSTRAWResponse() & 0x2) >> 1) == 1;

    EmulatedSTRAW_HLT_M3 = HLTevent->GetSTRAWBitOn(2);
    EmulatedSTRAW_HLT_M5 = HLTevent->GetSTRAWBitOn(1);
    EmulatedKTAG_HLT = HLTevent->GetKTAGResponse() == 1;

    fIsL0MT = EmulatedRICH && EmulatedQX; // MASK 5
    fIsL1MT = EmulatedKTAG_HLT && EmulatedSTRAW_HLT_M5;

    fIsL0M5MO1 = EmulatedRICH && EmulatedQX && EmulatedMO1; // MASK 5 MO1
    fIsL0M5LKr = EmulatedRICH && EmulatedQX && EmulatedLKr10; // MASK 5 LKr
    
    fIsL0M3 = EmulatedRICH && EmulatedQX && EmulatedMO1 && EmulatedLKr10; // MASK 3
    fIsL1M3 = EmulatedKTAG_HLT && EmulatedSTRAW_HLT_M3;

    if(!KTAGok || !STRAWok) {
      return;
    }
  }
  else {
    fIsL0MT = TriggerConditions::GetInstance()->L0TriggerOn(GetRunID(), GetL0Data(), fIDMT);
    fIsL1MT = TriggerConditions::GetInstance()->L1TriggerOnIgnoringFlagging(GetRunID(), GetL1Data(),
									    fIDMT);

    fIsL0M3 = TriggerConditions::GetInstance()->L0TriggerOn(GetRunID(), GetL0Data(), fIDM3);
    fIsL1M3 = TriggerConditions::GetInstance()->L1TriggerOnIgnoringFlagging(GetRunID(), GetL1Data(),
									    fIDM3);
  }
  //Double_t Timestamp = GetEventHeader()->GetTimeStamp() * ClockPeriod / 1.e9;

  fEventSelected = false;
  SetOutputState("EventSelected", kOValid);
  SetOutputState("VertexIndex", kOValid);
  SetOutputState("KaonMass", kOValid);
  SetOutputState("KaonMomt", kOValid);
  SetOutputState("VertexPos", kOValid);
  SetOutputState("VertexTime", kOValid);

  fBeamThreeMomt = BeamParameters::GetInstance()->GetBeamThreeMomentum();
  fTriggerTime = GetL0Data()->GetReferenceFineTime() * TriggerCalib;
  fNSTRAWTracks = GetEvent<TRecoSpectrometerEvent>()->GetNCandidates();
  FillHisto("hNTracks", fNSTRAWTracks);

  // external tools outputs
  fDownTrack = *(std::vector<DownstreamTrack> *)GetOutput("DownstreamTrackBuilder.Output");
  fVertices =
    *(std::vector<SpectrometerTrackVertex> *)GetOutput("SpectrometerVertexBuilder.Output3");
  fVertices2tr =
    *(std::vector<SpectrometerTrackVertex> *)GetOutput("SpectrometerVertexBuilder.Output2");

  if(GetWithMC()) {
    Event *evt = GetMCEvent();
    EventBoundary *evt0 = static_cast<EventBoundary *>(evt->GetEventBoundary(0));
    fTrueKaonP = static_cast<KinePart*>(evt->GetKineParts()->At(0))->GetFinal4Momentum();

    if(evt0->GetNKineParts())
      FillHisto("hTrueVtxZ", evt->GetKinePart(0)->GetEndPos().Z());
  }

  if(!ProcessEvent())
    return;

  // GTK Smearing
  TRecoGigaTrackerCandidate *gtk = 
    static_cast<TRecoGigaTrackerCandidate *>(GetEvent<TRecoGigaTrackerEvent>()->GetCandidate(
    fGTKCandIndex));

  fGTKMomt = gtk->GetMomentum();
  if(GetWithMC()) {
    fGTKMomt_smear = GTKSmearing(fGTKMomt);
  }
  else {
    fGTKMomt_smear = fGTKMomt;
  }

  // Chosen vertices histogram
  SpectrometerTrackVertex vtx = fVertices[fChosenVtxIndex];
  Double_t chi2 = vtx.GetChi2();
  FillHisto("hChosenVtxChi2", chi2);

  fvpt = vtx.GetTotalThreeMomentum().Pt(fGTKMomt) / 1e3;
  fvpt_smear = vtx.GetTotalThreeMomentum().Pt(fGTKMomt_smear);  
  fvtxZ = vtx.GetPosition().z();

  fKaonP.SetVectM(fGTKMomt, MKCH);
  Double_t ZMUV3 = GeometricAcceptance::GetInstance()->GetZMUV3();
  Double_t ZLKr = GeometricAcceptance::GetInstance()->GetZLKr();
  Double_t ZStraw[4] = {GeometricAcceptance::GetInstance()->GetZStraw(0),
    GeometricAcceptance::GetInstance()->GetZStraw(1),
    GeometricAcceptance::GetInstance()->GetZStraw(2),
    GeometricAcceptance::GetInstance()->GetZStraw(3)};

  fPosPion1XY.clear(); fPosPion2XY.clear(); fNegPionXY.clear();

  Int_t nofpospions = 0;
  
  for(Int_t i = 0; i < 3; i++) {
    TVector3 trackP = vtx.GetTrackThreeMomentum(i);
    TRecoSpectrometerCandidate *STRAWCand = fDownTrack[fTrackID.at(i)].GetSpectrometerCandidate();
    Double_t EoP = fDownTrack[fChosenTrackID.at(i)].GetLKrEoP();
    Double_t xAtMUV3FrontPlane = fDownTrack[fChosenTrackID.at(i)].xAt(ZMUV3);
    Double_t yAtMUV3FrontPlane = fDownTrack[fChosenTrackID.at(i)].yAt(ZMUV3);
    Double_t xAtLKrFrontPlane = fDownTrack[fChosenTrackID.at(i)].xAt(ZLKr);
    Double_t yAtLKrFrontPlane = fDownTrack[fChosenTrackID.at(i)].yAt(ZLKr);

    if(vtx.GetTrackCharge(i) == 1) {
      nofpospions++;
      FillHisto("hChosenPosPion1EoP", EoP);
      FillHisto("hChosenXYPosPion1ZMUV3", xAtMUV3FrontPlane, yAtMUV3FrontPlane);
      FillHisto("hChosenXYPosPion1ZLKr", xAtLKrFrontPlane,  yAtLKrFrontPlane);
      fPosPion1TotalQuality = STRAWCand->GetCombinationTotalQuality();
      fPosPion1HoughQuality = STRAWCand->GetCombinationHoughQuality();
      for(Int_t j = 0; j < 4; j++)
	fPosPion1XY.push_back({STRAWCand->xAt(ZStraw[j]), STRAWCand->yAt(ZStraw[j])});

      if(nofpospions == 2) {
        FillHisto("hChosenPosPion2EoP", EoP);
        FillHisto("hChosenXYPosPion2ZMUV3", xAtMUV3FrontPlane, yAtMUV3FrontPlane);
        FillHisto("hChosenXYPosPion2ZLKr", xAtLKrFrontPlane, yAtLKrFrontPlane);
        fPosPion2TotalQuality = STRAWCand->GetCombinationTotalQuality();
        fPosPion2HoughQuality = STRAWCand->GetCombinationHoughQuality();
        for(Int_t j = 0; j < 4; j++)
          fPosPion2XY.push_back({STRAWCand->xAt(ZStraw[j]), STRAWCand->yAt(ZStraw[j])});
      }
    }
    else {
      FillHisto("hChosenNegPionEoP", EoP);
      FillHisto("hChosenXYNegPionZMUV3", xAtMUV3FrontPlane, yAtMUV3FrontPlane);
      FillHisto("hChosenXYNegPionZLKr", xAtLKrFrontPlane, yAtLKrFrontPlane);
      fNegPionTotalQuality = STRAWCand->GetCombinationTotalQuality();
      fNegPionHoughQuality = STRAWCand->GetCombinationHoughQuality();
      for(Int_t j = 0; j < 4; j++)
        fNegPionXY.push_back({STRAWCand->xAt(ZStraw[j]), STRAWCand->yAt(ZStraw[j])});
    }
  }

  fPosPion1P3 = fPosPion1P.P();
  fPosPion2P3 = fPosPion2P.P();
  fNegPionP3 = fNegPionP.P();
  fInvMass = ((fPosPion1P + fPosPion2P + fNegPionP).M() - MKCH ) / 1e3;
  fMMissSq = (fKaonP - fPosPion1P - fPosPion2P - fNegPionP).M2() / 1e6;

  if(TriggerM5Condition())
    CutApply(fmask[0]);

  if(TriggerM3Condition()) {
    CutApply(fmask[1]);
  }

  if(TriggerM5Condition() || TriggerM3Condition())
    CutApply(fmask[2]);
}

void K3piMySelection::PostProcess() {
}

void K3piMySelection::EndOfBurstUser() {
}

void K3piMySelection::EndOfRunUser() {
}

void K3piMySelection::EndOfJobUser() {
  fMatchGTK->SaveAllPlots();
  SaveAllPlots();
}

Bool_t K3piMySelection::TriggerM5Condition() {
  return fIsL0MT && fIsL1MT;
}

Bool_t K3piMySelection::TriggerM3Condition() {
  return fIsL0M3 && fIsL1M3;
}

Double_t K3piMySelection::getLikelihood(Double_t pld, Double_t dt) {
  Double_t like_v =
    TMath::Gaus(dt, MeanDt, SigmaDt, true) * TMath::Landau(pld, MeanPld, SigmaPld, true);
  return like_v / like_m;
}

Bool_t K3piMySelection::tracksPassCuts() {
  Bool_t InAcceptance = true;
  for(Int_t i = 0; i < 3; i++) {
    DownstreamTrack trck = fDownTrack[fTrackID.at(i)];
    if(!GeometricAcceptance::GetInstance()->InAcceptance(&trck, NA62::kSpectrometer, 0)) {
      InAcceptance = false;
    }
    if(!GeometricAcceptance::GetInstance()->InAcceptance(&trck, NA62::kSpectrometer, 1)) {
      InAcceptance = false;
    }
    if(!GeometricAcceptance::GetInstance()->InAcceptance(&trck, NA62::kSpectrometer, 2)) {
      InAcceptance = false;
    }
    if(!GeometricAcceptance::GetInstance()->InAcceptance(&trck, NA62::kSpectrometer, 3)) {
      InAcceptance = false;
    }
    if(!GeometricAcceptance::GetInstance()->InAcceptance(&trck, NA62::kNewCHOD)) {
      InAcceptance = false;
    }
    if(!GeometricAcceptance::GetInstance()->InAcceptance(&trck, NA62::kCHOD)) {
      InAcceptance = false;
    }
    if(!GeometricAcceptance::GetInstance()->InAcceptance(&trck, NA62::kLKr)) {
      InAcceptance = false;
    }
    if(!GeometricAcceptance::GetInstance()->InAcceptance(&trck, NA62::kMUV3)) {
      InAcceptance = false;
    }
  }
  if(!InAcceptance)
    return false;

  fVertexTime = 0.;

  Bool_t VertexTriggerInTime(false);
  Bool_t AllTracksGoodEoP(true);
  Bool_t AllTracksGoodCh2(true);
  Bool_t AllTracksGoodMom(true);

  for(UInt_t j = 0; j < 3; j++) {
    Int_t TID = fTrackID.at(j);
    Double_t EoP = fDownTrack[TID].GetLKrEoP();
    FillHisto("hEoP", EoP);
    if(EoP > fCutEoPMaximal) {
      AllTracksGoodEoP = false;
    }

    Double_t Ch2 = fDownTrack[TID].GetChi2();
    FillHisto("hTrackChi2", Ch2);
    if(Ch2 > fCutTrackChi2) {
      AllTracksGoodCh2 = false;
    }

    Double_t Mom = fDownTrack[TID].GetMomentum();
    FillHisto("hTrackMomt", Mom);
    if(Mom < fCutTrackLowMomt || Mom > fCutTrackHighMomt) {
      AllTracksGoodMom = false;
    }

    Double_t BFAF = fabs(fDownTrack[TID].GetMomentum() - fDownTrack[TID].GetMomentumBeforeFit());
    if(BFAF > fCutTrackAfterBeforeFitMomDiff) {
      AllTracksGoodMom = false;
    }

    fTrackTime.at(j) = fVertices.at(fVtxIndex).GetTrackCHODTime(j);
  }
  if(!AllTracksGoodEoP)
    return false;
  if(!AllTracksGoodCh2)
    return false;
  if(!AllTracksGoodMom)
    return false;

  // vertex timing
  Int_t Nclose = 0;
  fVertexTime = fVertices.at(fVtxIndex).GetCHODTime();
  for(Int_t i = 0; i < 3; i++) {
    FillHisto("hTrackVertexTime", fTrackTime.at(i) - fVertexTime);
    if(fabs(fTrackTime.at(i) - fVertexTime) < fCutTrackVertexTime)
      Nclose++;
  }
  FillHisto("hNtracksInTimeWithVtx", Nclose);
  if(Nclose != 3) {
    return false;
  }

  FillHisto("hVertexTriggerTime", fVertexTime - fTriggerTime);
  if(fabs(fVertexTime - fTriggerTime) < fCutVertexTriggerTime) {
    VertexTriggerInTime = true;
  }
  if(!VertexTriggerInTime)
    return false;

  return true;
}

Bool_t K3piMySelection::isMuon(UInt_t j) {
  DownstreamTrack trck = fDownTrack[fTrackID.at(j)];
  Double_t vertexTime = fVertices[fVtxIndex].GetCHODTime();
  Bool_t isMuon = fDownTrack[fTrackID.at(j)].MUV3AssociationExists() &&
    fDownTrack[fTrackID.at(j)].MUV3InTimeOuterAssociationExists(vertexTime, 
    fCutMuonVertexMUV3Time) && fDownTrack[fTrackID.at(j)].GetLKrEoP() < fCutMuonLKrEoP;
  return isMuon;
}

Bool_t K3piMySelection::isPion(UInt_t j) {
  DownstreamTrack trck = fDownTrack[fTrackID.at(j)];
  Double_t vertexTime = fVertices[fVtxIndex].GetCHODTime();
  Bool_t isPion = !(fDownTrack[fTrackID.at(j)].MUV3InTimeAssociationExists(vertexTime,
    fCutPionVertexMUV3Time)) && fDownTrack[fTrackID.at(j)].GetLKrEoP() < fCutPionLKrEoP;
  return isPion;
}

TVector3 K3piMySelection::GTKSmearing(TVector3 vec) {
  Double_t x = vec.X();
  Double_t y = vec.Y();
  Double_t z = vec.Z();
  TVector3 vec_smear;
  //vec.SetXYZ(x + 0.495, y, z + 0.509 * (z - fTrueKaonP.Z()));
  //vec.SetXYZ(x + (1.8133/1.7397 - 1) * (x - fTrueKaonP.X()), y + (1.819/1.728 - 1) * (y - fTrueKaonP.Y()), z + (330./275.2 - 1) * (z - fTrueKaonP.Z()));
  vec_smear.SetXYZ(x + (1.09 - 1) * (x - fTrueKaonP.X())  + 0.407, y + (1.1058 - 1) * (y - fTrueKaonP.Y()),
    z + (1.443 - 1) * (z - fTrueKaonP.Z()) + 13.96);
  return vec_smear;
}

Bool_t K3piMySelection::ProcessEvent() {
  // get the number of good 3-track vertices in the event
  fNVertices = 0;
  fVtxIndex = -1;
  fChosenVtxIndex = -1;
  FillHisto("hVerticesSize", fVertices.size());
  
  for(UInt_t i = 0; i < fVertices.size(); i++) {
    TRecoGigaTrackerEvent *GTKevent = GetEvent<TRecoGigaTrackerEvent>();
    fMatchGTK->Process(fVertices[i], GTKevent);

    if(fMatchGTK->GetGTKMatchedSuccessfully())
      fGTKCandIndex = fMatchGTK->GetMatchedGTKTrackID();
    else
      continue;

    const SpectrometerTrackVertex& vtx = fMatchGTK->GetUpdatedSpectrometerTrackVertex();
    fVertices[i] = fMatchGTK->GetUpdatedSpectrometerTrackVertex();
    
    Double_t chrg = vtx.GetCharge();
    FillHisto("hVtxCharge", chrg);
    if(chrg != 1)
      continue;

    Double_t chi2 = vtx.GetChi2();
    FillHisto("hVtxChi2", chi2);
    if(chi2 > fCutVertexChi2)
      continue;
    Double_t vtxZ = vtx.GetPosition().z();
    FillHisto("hVtxZ", vtxZ);
    if(vtxZ < fCutZVertexMin || vtxZ > fCutZVertexMax)
      continue;
    fVtxIndex = i;

    Int_t nofpospions = 0;
    Int_t nofpions = 0;
    Bool_t firstP = false;

    for(UInt_t j = 0; j < 3; j++) {
      fTrackID.at(j) = fVertices[fVtxIndex].GetTrackIndex(j);
      fTrackCharge.at(j) = fDownTrack[fTrackID.at(j)].GetCharge();
      fTrackTime.at(j) = fDownTrack[fTrackID.at(j)].GetTrackTime();
      fTrackThreeMomt.at(j) = vtx.GetTrackThreeMomentum(j);

      if(fTrackCharge.at(j) == 1 && !firstP) {
        fPosPion1P.SetVectM(fTrackThreeMomt.at(j), MPI);
  	fPosPion1TrackID = fTrackID.at(j);
        firstP = true;
	
        if(isPion(j)) {
          nofpions++;
 	  nofpospions++;
	}
      }
      else if(fTrackCharge.at(j) == 1) {
    	fPosPion2P.SetVectM(fTrackThreeMomt.at(j), MPI);
  	fPosPion2TrackID = fTrackID.at(j);

        if(isPion(j)) {
          nofpions++;
 	  nofpospions++;
	}
      }

      if(fTrackCharge.at(j) == -1) {
        fNegPionP.SetVectM(fTrackThreeMomt.at(j), MPI);
 	fNegPionTrackID = fTrackID.at(j);

	if(isPion(j)) {
          nofpions++;
	}
      }
    }

    if(nofpions < 2 || (nofpions == 2 && nofpospions == 2)) {
      continue;
    }

    Bool_t tracksOK = tracksPassCuts();
    Double_t ktagtime = KTAGTime();
    if(!tracksOK || ktagtime == -9999.)
      continue;
    
    // Track separations in STRAW1 and LKr planes
    Double_t zstraw1 = GeometricAcceptance::GetInstance()->GetZStraw(0);
    for(Int_t ii = 0; ii < 2; ii++) {
      TRecoSpectrometerCandidate *Scand = fVertices[fVtxIndex].GetSpectrometerCandidate(ii);
      Double_t x1s = Scand->xAt(zstraw1);  // Straw1
      Double_t y1s = Scand->yAt(zstraw1);
      Double_t x1c = Scand->xAt(241093.0);  // LKr
      Double_t y1c = Scand->yAt(241093.0);
      for(Int_t jj = ii + 1; jj < 3; jj++) {
        TRecoSpectrometerCandidate *Scand2 = fVertices[fVtxIndex].GetSpectrometerCandidate(jj);
        Double_t x2s = Scand2->xAt(zstraw1);  // Straw1
        Double_t y2s = Scand2->yAt(zstraw1);
        Double_t x2c = Scand2->xAt(241093.0);  // LKr
        Double_t y2c = Scand2->yAt(241093.0);
        Double_t rs = sqrt((x1s - x2s) * (x1s - x2s) + (y1s - y2s) * (y1s - y2s));  // Straw1
        Double_t rc = sqrt((x1c - x2c) * (x1c - x2c) + (y1c - y2c) * (y1c - y2c));  // LKr
        FillHisto("hDistanceStraw1", rs);
        FillHisto("hDistanceLKr", rc);
      }
    }

    if(fVertices[fVtxIndex].GetMinTrackSeparationAtZ(zstraw1) < fCutStraw1Separation)
      continue;  // Straw1
    if(fVertices[fVtxIndex].GetMinTrackSeparationAtZ(241093.0) < fCutLKrSeparation)
      continue;  // LKr

    // N hits per STRAW candidate
    Bool_t goodcand = true;
    for(Int_t j = 0; j < 3; j++) {
      TRecoSpectrometerCandidate *STRAWCand =
        fDownTrack[fTrackID.at(j)].GetSpectrometerCandidate();
      FillHisto("hNStrawHitsInCandidate", STRAWCand->GetNHits());
      if(STRAWCand->GetNChambers() != 4)
        goodcand = false;
      else
        FillHisto("hNStrawHitsIn4CHCandidate", STRAWCand->GetNHits());

      FillHisto("hLKrDDeadCell", fDownTrack[fTrackID.at(j)].GetLKrClusterDDeadCell());
      if(fDownTrack[fTrackID.at(j)].GetLKrClusterDDeadCell() < fCutLKrClusterDDeadCell)
        goodcand = false;
    }
    if(!goodcand)
      continue;

    FillHisto("hNVertices", fNVertices);

    if(fIsL0M5MO1)
      FillHisto("hMask5MO1", 0.5);

    if(fIsL0M5LKr)
      FillHisto("hMask5LKr", 0.5);
	
    // this is a good vertex, remember important variables
    fNVertices++;
    fChosenVtxIndex = fVtxIndex;
    fChosenVertexTime = fVertexTime;
    fChosenKTAGTime = ktagtime;

    for(UInt_t j = 0; j < 3; j++) {
      fChosenTrackID.at(j) = fTrackID.at(j);
      fChosenTrackTime.at(j) = fTrackTime.at(j);
    }
  }

  // require exactly one good 3-track vertex
  if(fNVertices != 1)
    return false;
  else
    return true;
}

Double_t K3piMySelection::KTAGTime() {
  TRecoCedarEvent *CedarEvent = GetEvent<TRecoCedarEvent>();
  Double_t KTAGTimetoVtx{ -9999. };
  if(CedarEvent->GetNCandidates() == 0)
    return KTAGTimetoVtx;

  for(Int_t iCand = 0; iCand < CedarEvent->GetNCandidates(); iCand++) {
    TRecoCedarCandidate *CedarCand =
      static_cast<TRecoCedarCandidate *>(CedarEvent->GetCandidate(iCand));
    if(CedarCand->GetNSectors() < 5) {
      continue;
    }
    Double_t KTAGTime = CedarCand->GetTime();
    FillHisto("hTriggerKTAGTime", fTriggerTime - KTAGTime);
    if(fabs(fTriggerTime - KTAGTime) > fCutTriggerKTAGTime) {
      continue;
    }
    FillHisto("hVertexKTAGTime", fVertexTime - KTAGTime);
    if(fabs(fVertexTime - KTAGTime) > fCutVertexKTAGTime) {
      continue;
    }
    if(fabs(fVertexTime - KTAGTime) < fabs(fVertexTime - KTAGTimetoVtx))
      KTAGTimetoVtx = KTAGTime;
  }
  return KTAGTimetoVtx;
}

void K3piMySelection::FillHistos(TString mask, TString cut) {
  FillHisto(mask + cut + "/hGTKMomtX", fGTKMomt.X());
  FillHisto(mask + cut + "/hGTKMomtY", fGTKMomt.Y());
  FillHisto(mask + cut + "/hGTKMomtZ", fGTKMomt.Z());
  FillHisto(mask + cut + "/h3PiMomtX", fPosPion1P.X() + fPosPion2P.X() + fNegPionP.X());
  FillHisto(mask + cut + "/h3PiMomtY", fPosPion1P.Y() + fPosPion2P.Y() + fNegPionP.Y());
  FillHisto(mask + cut + "/h3PiMomtZ", fPosPion1P.Z() + fPosPion2P.Z() + fNegPionP.Z());    
  FillHisto(mask + cut + "/hGTK3PiMomtX", fGTKMomt.X() - fPosPion1P.X() + fPosPion2P.X() + fNegPionP.X());
  FillHisto(mask + cut + "/hGTK3PiMomtY", fGTKMomt.Y() - fPosPion1P.Y() + fPosPion2P.Y() + fNegPionP.Y());
  FillHisto(mask + cut + "/hGTK3PiMomtZ", fGTKMomt.Z() - fPosPion1P.Z() + fPosPion2P.Z() + fNegPionP.Z());
  FillHisto(mask + cut + "/hGTK3PiMomtZabs", fabs(fGTKMomt.Z() - fPosPion1P.Z() - fPosPion2P.Z() - fNegPionP.Z()));    
  FillHisto(mask + cut + "/hVtxPt", fvpt);
  FillHisto(mask + cut + "/hGTK3PiMomtX_smear", fGTKMomt_smear.X() - fPosPion1P.X() + fPosPion2P.X() + fNegPionP.X());
  FillHisto(mask + cut + "/hGTK3PiMomtY_smear", fGTKMomt_smear.Y() - fPosPion1P.Y() + fPosPion2P.Y() + fNegPionP.Y());
  FillHisto(mask + cut + "/hGTK3PiMomtZ_smear", fGTKMomt_smear.Z() - fPosPion1P.Z() + fPosPion2P.Z() + fNegPionP.Z());
  FillHisto(mask + cut + "/hGTK3PiMomtZabs_smear", fabs(fGTKMomt_smear.Z() - fPosPion1P.Z() - fPosPion2P.Z()
    - fNegPionP.Z()));
  FillHisto(mask + cut + "/hVtxPt_smear", fvpt_smear);
  
  FillHisto(mask + cut + "/hdGTKMomt", (fGTKMomt - fBeamThreeMomt).Mag() / 1e3);
  FillHisto(mask + cut + "/hPosPion1TotalQuality", fPosPion1TotalQuality);
  FillHisto(mask + cut + "/hPosPion2TotalQuality", fPosPion2TotalQuality);
  FillHisto(mask + cut + "/hNegPionTotalQuality", fNegPionTotalQuality);
  FillHisto(mask + cut + "/hVtxMomtVsPt", fvpt,
    (fVertices[fChosenVtxIndex].GetTotalThreeMomentum() - fGTKMomt).Mag() / 1e3);
  FillHisto(mask + cut + "/hChosenPosPion1Momt", fPosPion1P3 / 1e3);
  FillHisto(mask + cut + "/hChosenPosPion2Momt", fPosPion2P3 / 1e3);
  FillHisto(mask + cut + "/hChosenNegPionMomt", fNegPionP3 / 1e3);
  FillHisto(mask + cut + "/hChosenVtxPt", fvpt);
  FillHisto(mask + cut + "/hChosenVtxMomt", (fVertices[fChosenVtxIndex].GetTotalThreeMomentum() -
    fGTKMomt).Mag() / 1e3);
  FillHisto(mask + cut + "/hChosenVtxZ", fvtxZ);
  FillHisto(mask + cut + "/hChosenMM2PiPiMuVsVtxZ", fvtxZ, fInvMass);
  FillHisto(mask + cut + "/hInvMass", fInvMass);
  FillHisto(mask + cut + "/hMM2PiPiPi", fMMissSq);
  FillHisto(mask + cut + "/hChosenVtxPtVs3PiKaon", fInvMass, fvpt);
  FillHisto(mask + cut + "/hGTKMomt", fGTKMomt.Mag() / 1e3);
  FillHisto(mask + cut + "/hNLKrClusters", fNClusters);
  FillHisto(mask + cut + "/hNInTimeLKrClusters", fNInTimeClusters);
  FillHisto(mask + cut + "/hTotalClusterEnergyVsNInTimeLKrClusters", fNInTimeClusters,
    0.001 * fTotalClusterEnergy);
  FillHisto(mask + cut + "/hNotMatchedClustersEnergyVsNotMatchedLKrClusters",
    fInTimeNotMatchedNClusters, 0.001 * fTotalNotMatchedClusterEnergy);
  FillHisto(mask + cut + "/hPosPion1HoughQuality", fPosPion1HoughQuality);
  FillHisto(mask + cut + "/hPosPion2HoughQuality", fPosPion2HoughQuality);
  FillHisto(mask + cut + "/hNegPionHoughQuality", fNegPionHoughQuality);
}

void K3piMySelection::CutApply(TString mask) {  
  std::vector<Bool_t> cutconditions{
    abs(fGTKMomt.Mag() - fBeamThreeMomt.Mag()) / 1.e3 < fCutdGTKMomt,
    (pow(fInvMass, 2)/pow(fCutEllipseA, 2) + pow(fvpt - fCutEllipseY, 2)/pow(fCutEllipseB, 2)) < 1,
    (fPosPion1TotalQuality < fCutTotalQuality) && (fPosPion2TotalQuality < fCutTotalQuality)
    && (fNegPionTotalQuality < fCutTotalQuality),
    abs(fInvMass) < fCutM3Pi,
    false};
  
  for(UInt_t i = 0; i < fcut.size(); i++) {
    FillHistos(mask, fcut[i]);

    if(!cutconditions[i])
      return;
  }
}
