#include "Kmu4StrictSelection.hh"

#include "BeamParameters.hh"
#include "Event.hh"
#include "GeometricAcceptance.hh"
#include "KaonDecayConstants.hh"
#include "L0PrimitiveHandler.hh"
#include "MatchingGTKtoStrawVertex.hh"
#include "NA62ConditionsService.hh"
#include "Persistency.hh"
#include "TriggerConditions.hh"
#include "VertexLSF.hh"
#include "functions.hh"

#include <TGraphAsymmErrors.h>
#include <TMath.h>

#include <iostream>
#include <stdlib.h>

using namespace NA62Analysis;
using namespace TMath;

Kmu4StrictSelection::Kmu4StrictSelection(Core::BaseAnalysis *ba):
    Analyzer(ba, "Kmu4StrictSelection"), fPLD(std::make_unique<PointLineDistance>()),
    ftlcda(std::make_unique<TwoLinesCDA>()), fPrimitiveHandler(GetL0PrimitiveHandler()),
    fVertexTime(-99.), hdGTK(std::make_unique<TH3D>()),
    gProbLKr10(std::make_unique<TGraphAsymmErrors>()),
    hLKrPosPionClustersEnergyVsMomentum(std::make_unique<TH2D>()),
    hLKrNegPionClustersEnergyVsMomentum(std::make_unique<TH2D>()),
    hLKrPosMuonClustersEnergyVsMomentum(std::make_unique<TH2D>()), fWeight(1.), fdGTK_w(1.) {

  RequestL0Data();
  RequestL1Data();
  RequestHLTData();
  RequestBeamSpecialTrigger();
  RequestBeamData();
  RequestL0SpecialTrigger();

  RequestTree(new TRecoSpectrometerEvent, "Reco");
  RequestTree(new TRecoGigaTrackerEvent, "Reco");
  RequestTree(new TRecoCedarEvent, "Reco");
  RequestTree(new TRecoLKrEvent, "Reco");
  RequestTree(new TRecoCHODEvent, "Reco");
  RequestTree(new TRecoNewCHODEvent, "Reco");

  // all cuts as parameters
  AddParam("CutTrackLowMomt", &fCutTrackLowMomt, 10000.);
  AddParam("CutTrackHighMomt", &fCutTrackHighMomt, 50000.);
  AddParam("CutTrackChi2", &fCutTrackChi2, 100.);
  AddParam("CutTrackAfterBeforeFitMomDiff", &fCutTrackAfterBeforeFitMomDiff, 20000.);
  AddParam("CutVertexChi2", &fCutVertexChi2, 20.);
  AddParam("CutZVertexMin", &fCutZVertexMin, 110000.);
  AddParam("CutZVertexMax", &fCutZVertexMax, 180000.);
  AddParam("CutTotalVertexMomtWrtBeam", &fCutTotalVertexMomtWrtBeam, 2.2);
  AddParam("CutVertexPt", &fCutVertexPt, 0.01);
  AddParam("CutStraw1Separation", &fCutStraw1Separation, 15.0);
  AddParam("CutLKrSeparation", &fCutLKrSeparation, 200.0);
  AddParam("CutVertexTriggerTime", &fCutVertexTriggerTime, 5.);
  AddParam("CutVertexKTAGTime", &fCutVertexKTAGTime, 5.);
  AddParam("CutTriggerKTAGTime", &fCutTriggerKTAGTime, 5.);
  AddParam("CutTrackVertexTime", &fCutTrackVertexTime, 5.);
  AddParam("CutVertexMUV3Time", &fCutMuonVertexMUV3Time, 5.);
  AddParam("CutVertexMUV3Time", &fCutPionVertexMUV3Time, 10.);
  AddParam("CutMuonLKrEoP", &fCutMuonLKrEoP, 0.2);
  AddParam("CutPionLKrEoP", &fCutPionLKrEoP, 0.9);
  AddParam("CutLKrClusterDistDeadCell", &fCutLKrClusterDDeadCell, 20);

  // Cuts on background
  AddParam("CutdGTKMomentum", &fCutdGTKMomt, 3.);
  AddParam("CutEllipseY", &fCutEllipseY, 0.01275);
  AddParam("CutEllipseA", &fCutEllipseA, 0.01235);
  AddParam("CutEllipseB", &fCutEllipseB, 0.0136167);
  AddParam("CutPosMuTotalQuality", &fCutPosMuTotalQuality, 1.49);
  AddParam("CutRSmu", &fCutRSmu, 0.0235);
  AddParam("CutL1Smu", &fCutL1Smu, 0.012);
  AddParam("CutL1Smu", &fCutL2Smu, 0.016);
  AddParam("CutCosTheta", &fCutCost, 0.7);
  AddParam("CutPiPiMuMisID", &fCutMM2PiPiMu_misID, -0.00025);

  // Weights
  AddParam("WeightPosPionEoP", &fWeightPosPionEoP, 0.981729);
  AddParam("WeightNegPionEoP", &fWeightNegPionEoP, 0.982835);
  AddParam("WeightPosMuonEoP", &fWeightPosMuonEoP, 0.890295);

  fMatchGTK = std::make_unique<MatchingGTKtoStrawVertex>(ba, this, "MatchGTK");
  //fMatchGTK->SetGTKMatchingAlgorithm(MatchingGTKtoStrawVertex::kChi2); // or kChi2
  fMatchGTK->SetGTKMatchingAlgorithm(MatchingGTKtoStrawVertex::kLikelihood);
  fMatchGTK->SetGTKMatchingMinLikelihood(0.01); 
  fMatchGTK->SetGTKMatchingMaxDtWithVertex(1.);    // ns
  // Choose the appropriate GTK matching quality cut from the two below
  //fMatchGTK->SetGTKMatchingMaxChi2(20.);           // sensible interval may be (10, 50)
}

Kmu4StrictSelection::~Kmu4StrictSelection() {
}

void Kmu4StrictSelection::InitOutput() {
  // registering outputs
  RegisterOutput("EventSelected", &fEventSelected);
  RegisterOutput("VertexIndex", &fChosenVtxIndex);
  RegisterOutput("VertexTime", &fChosenVertexTime);
}

void Kmu4StrictSelection::InitHist() {
  BookHisto(new TH1F("hNRuns", "Number of runs", 1, 0, 1));
  BookHisto(new TH1F("hNBursts", "Number of bursts", 1, 0, 1));
  BookHisto(new TH1F("hVerticesSize", "Number of vertices per event", 5, 0, 5));
  BookHisto(new TH1F("hTrueVtxZ", "True vertex Z; z [mm]", 300, 0., 300000.));
  BookHisto(new TH1F("hEmulatedRICH", "Number of RICH primitives", 1, 0, 1));
  BookHisto(new TH1F("hEmulatedQX", "Number of QX primitives", 1, 0, 1));
  BookHisto(new TH1F("hEmulatedMO1", "Number of MO1 primitives", 1, 0, 1));
  BookHisto(new TH1F("hEmulatedLKr10", "Number of LKr10 primitives", 1, 0, 1));
  BookHisto(new TH1F("hEmulatedSTRAWM3", "Number of STRAW mask 3 primitives", 1, 0, 1));
  BookHisto(new TH1F("hEmulatedSTRAWM5", "Number of STRAW mask 5 primitives", 1, 0, 1));  
  BookHisto(new TH1F("hNTracks", "Number of reconstructed tracks per event", 20, -0.5, 19.5));
  BookHisto(new TH1F("hVtxCharge", "Vertex charge", 11, -5.5, 5.5));
  BookHisto(new TH1F("hVtxChi2", "Vertex chi2", 100, 0., 100.));
  BookHisto(new TH1F("hVtxZ", "Vertex Z; z [mm]", 200, 100000., 200000.));


  //fmask = {"mask5", "mask3", "mask3ORmask5"}; "/AfterPtVsNuMomt"
  std::vector<TString> PIDSwap = {"", "PIDSwap"};
  fmask = {"/mask3"};
  fcut = {{"/Original", "/AfterdMomtGTK", "/AfterEllipse", "/AfterQuality", "/AfterCost",
      "/AfterPiPiMuMisID", "/AfterRParSmu", "/AfterLParSmu", "/AfterParSmu"},
          {"/All", "/ExceptdMomtGTK", "/ExceptEllipse", "/ExceptQuality", "/ExceptCost",
	   "/ExceptPiPiMuMisID", "/ExceptRParSmu", "/ExceptLParSmu", "/ExceptParSmu"}};
  fsr = {"", "/SR"};

  for(UInt_t n = 0; n < PIDSwap.size(); n++) {
    BookHisto(new TH1F(PIDSwap[n] + "/hDistanceStraw1", "Distance between track pairs in Straw1;[mm]",
      500, 0, 1000));
    BookHisto(new TH1F(PIDSwap[n] + "/hDistanceLKr", "Distance between track pairs in LKr;[mm]", 500, 0, 2000));
    BookHisto(new TH1F(PIDSwap[n] + "/hNStrawHitsInCandidate", "Number of hits in candidate", 50, 0, 50));
    BookHisto(new TH1F(PIDSwap[n] + "/hNStrawHitsIn4CHCandidate", "Number of hits in 4CH candidate", 50, 0, 50));
    BookHisto(new TH1F(PIDSwap[n] + "/hLKrDDeadCell",
      "Track distance to nearest dead cell;Distance to deal cell [mm]", 150, 0., 3000.));
    BookHisto(new TH1F(PIDSwap[n] + "/hNVertices", "Number of good 3-track vertices", 10, 0, 10));
    BookHisto(new TH1F(PIDSwap[n] + "/hTrackChi2", "Track chi2", 50, 0., 500.));
    BookHisto(new TH1F(PIDSwap[n] + "/hTrackMomt", "Track p", 80, 0., 80000.));
    BookHisto(new TH1F(PIDSwap[n] + "/hNtracksInTimeWithVtx", "Number of tracks in-time with vertex",
      4, -0.5, 3.5));
    BookHisto(new TH1F(PIDSwap[n] + "/hTrackVertexTime", "Track - vertex time difference", 200, -20., 20.));
    BookHisto(new TH1F(PIDSwap[n] + "/hVertexTriggerTime", "Vertex - trigger time difference", 200, -20., 20.));
    BookHisto(new TH1F(PIDSwap[n] + "/hTriggerKTAGTime", "Trigger - KTAG time difference", 200, -20., 20.));
    BookHisto(new TH1F(PIDSwap[n] + "/hVertexKTAGTime", "Vertex - KTAG time difference", 200, -20., 20.));
    BookHisto(new TH1F(PIDSwap[n] + "/hChosenVtxChi2", "Chosen Vertex chi2", 100, 0., 100.));
    BookHisto(new TH1F(PIDSwap[n] + "/hChosenPosPionEoP", "Chosen Positive Pion Track E/p", 130, 0., 1.3));
    BookHisto(new TH1F(PIDSwap[n] + "/hChosenNegPionEoP", "Chosen Negative Pion Track E/p", 130, 0., 1.3));
    BookHisto(new TH1F(PIDSwap[n] + "/hChosenPosMuonEoP", "Chosen Positive Muon Track E/p", 130, 0., 1.3));
    BookHisto(new TH2F(PIDSwap[n] + "/hChosenXYPosPionZMUV3",
      "Chosen Positive Pion XY position in MUV3 front plane; X [mm]; Y [mm]", 400, -2000., 2000.,
									      400, -2000., 2000));
    BookHisto(new TH2F(PIDSwap[n] + "/hChosenXYNegPionZMUV3",
      "Chosen Negative Pion XY position in MUV3 front plane; X [mm]; Y [mm]", 400, -2000., 2000.,
									      400, -2000., 2000));
    BookHisto(new TH2F(PIDSwap[n] + "/hChosenXYPosMuonZMUV3",
      "Chosen Positive Muon XY position in MUV3 front plane; X [mm]; Y [mm]", 400, -2000., 2000.,
									      400, -2000., 2000));
    BookHisto(new TH2F(PIDSwap[n] + "/hChosenXYPosPionZLKr",
      "Chosen Positive Pion XY position in LKr front plane; X [mm]; Y [mm]", 400, -2000., 2000.,
									     400, -2000., 2000));
    BookHisto(new TH2F(PIDSwap[n] + "/hChosenXYNegPionZLKr",
      "Chosen Negative Pion XY position in LKr front plane; X [mm]; Y [mm]", 400, -2000., 2000.,
									     400, -2000., 2000));
    BookHisto(new TH2F(PIDSwap[n] + "/hChosenXYPosMuonZLKr",
      "Chosen Positive Muon XY position in LKr front plane; X [mm]; Y [mm]", 400, -2000., 2000.,
									     400, -2000., 2000));
    BookHisto(new TH1F(PIDSwap[n] + "/LKr/hLKrClusterTime", "LKr cluster time wrt trigger;Time [ns]",
      200, -50, 50));
    BookHisto(new TH2F(PIDSwap[n] + "/LKr/hLKrPosPionClustersEnergyVsMomt", ";p [GeV/c];Energy [GeV]",
      200, 0., 100., 300, 0., 100.));
    BookHisto(new TH2F(PIDSwap[n] + "/LKr/hLKrNegPionClustersEnergyVsMomt", ";p [GeV/c];Energy [GeV]",
      200, 0., 100., 300, 0., 100.));
    BookHisto(new TH2F(PIDSwap[n] + "/LKr/hLKrPosMuonClustersEnergyVsMomt", ";p [GeV/c];Energy [GeV]",
      200, 0., 100., 300, 0., 100.));  
    BookHisto(new TH1F(PIDSwap[n] + "/LKr/hLKrClusterEnergy", "LKr cluster energy;Energy [GeV]", 100, 0, 100));
    BookHisto(new TH1F(PIDSwap[n] + "/LKr/hLKrClusterEnergyLKr10", "LKr cluster energy;Energy [GeV]", 100, 0, 100));
    BookHisto(new TH1F(PIDSwap[n] + "/LKr/hLKrClusterEnergyM3", "LKr cluster energy;Energy [GeV]", 100, 0, 100));
    BookHisto(new TH1F(PIDSwap[n] + "/LKr/hLKrClusterEnergyM5LKr10", "LKr cluster energy;Energy [GeV]",
      100, 0, 100));
    BookHisto(new TH1F(PIDSwap[n] + "/LKr/hLKrClusterEnergyM5", "LKr cluster energy;Energy [GeV]", 100, 0, 100));
    BookHisto(new TH2F(PIDSwap[n] + "/LKr/hLKr10VsLKrClusterEnergy", "Trigger LKr10 vs LKr cluster energy;"
      "Energy [GeV]", 100, 0, 100, 2, 0, 2));
    BookHisto(new TH1F(PIDSwap[n] + "/LKr/hLKrClusterEnergyAll", "LKr cluster energy;Energy [GeV]", 100, 0, 100));
    BookHisto(new TH1F(PIDSwap[n] + "/LKr/hLKrClusterEnergyTrue", "LKr cluster energy;Energy [GeV]", 100, 0, 100));
  
    for(UInt_t k = 0; k < fmask.size(); k++) {
      for(UInt_t j = 0; j < 2; j++) {
	for(UInt_t i = 0; i < fcut[j].size(); i++) {
	  for(UInt_t l = 0; l < fsr.size(); l++) {
	    BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + fsr[l] + "/hdGTKMomt", "|#vec{p}_{K^{+},GTK} - "
	      "#vec{p}_{beam}|; p [GeV/c]", 300, 0., 15.));
	    BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + fsr[l] + "/hMM2PiPiVsPionRFMuonP",
	      "(p_{K^{+}}-p_{#pi^{+}}-p_{#pi^{-}})^{2} vs |#vec{p}_{#mu}| in decayed #pi RF",
	      300, 0, 100., 300, -0.05, 0.05));
	    BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + fsr[l] + "/hChosenVtxPtVs3PiKaon", 
	      "Pt vs m_{3#pi}-m_{K^{+}};m_{3#pi} - m_{K^{+}} [GeV/c^{2}];P_{t} [GeV/c]", 300, -0.07, 0.07,
	      300, 0., 100. / 1e3));
	    BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + fsr[l] + "/hPosPiTotalQuality",
	      "Max track total quality", 300, 0., 5.));
	    BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + fsr[l] + "/hNegPiTotalQuality",
	      "Max track total quality", 300, 0., 5.));
	    BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + fsr[l] + "/hPosMuTotalQuality",
              "Max track total quality", 300, 0., 5.));
	    BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + fsr[l] + "/hChosenMM2PiPi",
	      "Chosen vertex Missing mass squared (K-#pi^{+}-#pi^{-})^{2}; [GeV^{2}/c^{4}]", 300, -0.05, 0.05));
	    BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + fsr[l] + "/hChosenDiPionVsDiLepton", 
	      "(p_{#pi^{+}}+p_{#pi^{-}})^{2} vs (p_{K^{+}}-p_{#pi^{+}}-p_{#pi^{-}})^{2};S_{#mu} [GeV^{2}/c^{4}];"
	      "m^{2}_{2#pi} [GeV^{2}/c^{4}]", 300, 0., 0.05, 300, 0.05, 0.18));
	    if(l == 1) {
	      BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + fsr[l] + "/hChosenCostVsMM2PiPiMu",
		"cos(#theta) vs Chosen Vertex (p_{K^{+}}-p_{#pi^{+}}-p_{#pi^{-}}-p_{#mu^{+}})^{2};"
		"m^{2}_{miss} [GeV^{2}/c^{4}];cos(#theta)", 300, -0.001, 0.001, 300, -1., 1.));
	      BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + fsr[l] + "/hChosenMM2PiPiMuVsPiPiMu_misID",
		"(p_{K^{+}}-p_{#pi^{+}}-p_{#pi^{-}}-p_{#mu^{+}}(m_{#pi}))^{2} "
		"vs (p_{K^{+}}-p_{#pi^{+}}-p_{#pi^{-}}-p_{#mu^{+}})^{2};m^{2}_{miss} [GeV^{2}/c^{4}];"
		"m^{2}_{miss}(#pi) [GeV^{2}/c^{4}]", 300, -0.001, 0.001, 300, -0.011, 0.006));
	    }
	    else {
	      BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + fsr[l] + "/hChosenCostVsMM2PiPiMu",
		"cos(#theta) vs Chosen Vertex (p_{K^{+}}-p_{#pi^{+}}-p_{#pi^{-}}-p_{#mu^{+}})^{2};"
		"m^{2}_{miss} [GeV^{2}/c^{4}];cos(#theta)", 300, -0.006, 0.006, 300, -1., 1.));
	      BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + fsr[l] + "/hChosenMM2PiPiMuVsPiPiMu_misID",
		"(p_{K^{+}}-p_{#pi^{+}}-p_{#pi^{-}}-p_{#mu^{+}}(m_{#pi}))^{2} "
		"vs (p_{K^{+}}-p_{#pi^{+}}-p_{#pi^{-}}-p_{#mu^{+}})^{2};m^{2}_{miss} [GeV^{2}/c^{4}];"
		"m^{2}_{miss}(#pi) [GeV^{2}/c^{4}]", 300, -0.006, 0.006, 300, -0.011, 0.006));
	    }
	    BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + fsr[l] + "/hVtxPtVsNuMomt", 
	      "Pt vs |#vec{p_{#nu}}|;|#vec{p}_{miss}| [GeV/c];P_{t} [GeV/c]", 300, 0., 60., 300, 0., 100. / 1e3));
	    BookHisto(new TH3F(PIDSwap[n] + fmask[k] + fcut[j][i] + fsr[l] + "/hChosenVtxPtVs3PiKaonVsNuMomt", 
	      ";m_{3#pi} - m_{K^{+}} [GeV/c^{2}];P_{t} [GeV/c];|#vec{p}_{miss}| [GeV/c]", 100, -0.07, 0.07,
	      100, 0., 100. / 1e3, 100, 0., 60.));
	  }
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hNLKrClusters",
	    "Number of LKr clusters;Number of clusters", 10, -0.5, 9.5));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hNInTimeLKrClusters",
	    "Number of LKr clusters;Number of clusters", 10, -0.5, 9.5));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hTotalClusterEnergyVsNInTimeLKrClusters",
	    "Total energy of in time clusters vs no of such clusters", 10, -0.5, 9.5, 200, 0., 100.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] +
	    "/hNotMatchedClustersEnergyVsNotMatchedLKrClusters",
	    "Total energy of not matched in time clusters vs no of such clusters", 10, -0.5, 9.5, 200, 0., 100.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hSlopeDiff",
	    "(#theta_{Y,bm} - #theta_{Y,am})", 400, -0.001, 0.001));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hVtxMomtVsPt",
	    "|#vec{p_{vtx}} - #vec{p_{GTK}}| vs pt; p [GeV/c];p [GeV/c]", 300, 0., 100. / 1e3,
	    300, 0., 50.));

	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hChosenPosPionMomt",
	    "Chosen Positive Pion p; p [GeV/c]", 80, 0., 80.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hChosenNegPionMomt",
	    "Chosen Negative Pion p; p [GeV/c]", 80, 0., 80.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hChosenPosMuonMomt",
	    "Chosen Positive Muon p; p [GeV/c]", 80, 0., 80.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hChosenMM2PiPiMuVsPiPi", 
	    "Chosen vertex (K-#pi^{+}-#pi^{-}-#mu^{+})^{2} vs (K-#pi^{+}-#pi^{-})^{2}; [GeV^{2}/c^{4}]",
	    300, -0.02, 0.05, 300, -0.05, 0.03));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hChosenMM2PiPiMu", 
	    "Chosen vertex Missing mass squared (K-#pi^{+}-#pi^{-}-#mu^{+})^{2}; m^{2}_{miss} [GeV^{2}/c^{4}]",
	    600, -0.04, 0.04));
  	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hChosenMM2PiPiMu_now", 
	    "Chosen vertex Missing mass squared (K-#pi^{+}-#pi^{-}-#mu^{+})^{2}; m^{2}_{miss} [GeV^{2}/c^{4}]",
	    600, -0.04, 0.04));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hChosenNuMomt", 
	    "Chosen |#vec{p_{#nu}}|; p [GeV/c]", 100, 0., 60.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hChosenMM2PiPiMuVsMu", 
	    "Chosen vertex (K-#pi^{+}-#pi^{-}-#mu^{+})^{2} vs (K-#mu^{+})^{2}; [GeV^{2}/c^{4}]",
	    300, 0.05, 0.15, 300, -0.05, 0.03));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hChosenVtxPt",
	    "Chosen Vertex pt; p [GeV/c]", 300, 0., 100. / 1e3));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hChosenVtxMomt",
	    "Chosen |#vec{p_{vtx}} - #vec{p_{GTK}}|; [GeV/c]", 300, 0., 20.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hChosenVtxZ",
	     "Chosen Vertex Z; z [mm]", 200, 100000., 200000.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hChosenMM2PiPiMuVsNuMomt", 
	    "Chosen vertex (K-#pi^{+}-#pi^{-}-#mu^{+})^{2} vs |#vec{p_{#nu}}|; p [GeV/c];"
	    "[GeV^{2}/c^{4}]", 300, 0., 60., 300, -0.02, 0.02));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hChosenNuMomtVsVtxZ", 
	    "Chosen vertex |#vec{p_{#nu}}| vs Vertex Z; z [mm]", 200, 110000., 180000., 100, 0., 60.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hChosenMM2PiPiMuVsVtxZ", 
	    "Chosen Vertex (K^{+}-#pi^{+}-#pi^{-}-#mu^{+})^{2} vs Vertex Z;"
	    "z [mm]; [GeV^{2}/c^{4}]", 300, 110000, 180000, 300, -0.02, 0.02));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hChosenMM2PiPiVsPt",
	    "Chosen Vertex (K^{+}-#pi^{+}-#pi^{-})^{2} vs Pt", 300, 0., 100. / 1e3,
	    300, -0.01, 0.05));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hChosenMM2PiPiMuVsPt",
	    "Chosen Vertex (K^{+}-#pi^{+}-#pi^{-}-#mu^{+})^{2} vs Pt", 300, 0., 100. / 1e3,
	    300, -0.006, 0.006));	
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hGTKMomt", "|#vec{p}_{K^{+},GTK}|; p [GeV/c]",
	    300, 50., 100.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hTrueKaonMomt", "|#vec{p}_{K^{+},true}|;"
	    "p [GeV/c]", 300, 50., 100.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hdGTKTrueKaonMomt",
	    "|#vec{p}_{K^{+},GTK} - #vec{p}_{K^{+},true}|; p [MeV/c]", 500, 0., 10000.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hGTKMuonCDA", "CDA GTK and #mu", 200, 0., 40.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hPLDMuonGTK2Pi",
	    "PLD K^{+}+#pi^{+}+#pi^{-} and #mu", 200, 0., 40.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hPionRFMuonP",
	    "|#vec{p}_{#mu}| in decayed #pi RF; [MeV/c]", 300, 0, 100.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hCostVsPionRFMuonP",
	    "cos(#theta) vs |#vec{p}_{#mu}| in decayed #pi RF", 300, 0, 100., 300, -1., 1.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hChosenVtxTr2", "Chosen Vertex tr2", 1, 0, 1));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/h2TrVtxdZ", "2 Track vertex Z difference;"
	    "z [mm]", 200, -50000., 50000.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/h2TrMaxVtxdZ", "2 Track vertex Z difference;"
	    "z [mm]", 200, -50000., 50000.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hMisGTKIndex", "Mismatched GTK index tr2",
	    4, 0, 4));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hPLDDistanceMuonPair", "PLD Muon and 2#pi Vtx",
	    200, 0., 50.));

	  /* 2D histos for K3pi cut optimization */
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hPosPiHoughQuality", "Max track Hough quality",
	    300, 0., 10.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hNegPiHoughQuality", "Max track Hough quality",
	    300, 0., 10.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/hPosMuHoughQuality", "Max track Hough quality",
	    300, 0., 10.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/ch0/hdDecayedPionMuonXY", "Y vs X",
	    400, -800., 800., 400, -800., 800.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/ch0/hdDecayedPionMuonXYcond",
	    "Y vs X ch0 < 20mm, ch1 < 50mm", 400, -50., 50., 400, -50., 50.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/ch0/hPosPiXY", "Y vs X", 300, -1000., 1000.,
	    300, -1000., 1000.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/ch0/hNegPiXY", "Y vs X", 300, -1000., 1000.,
	    300, -1000., 1000.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/ch0/hPosMuXY", "Y vs X", 300, -1000., 1000.,
	    300, -1000., 1000.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/ch1/hdDecayedPionMuonXY", "Y vs X",
	    400, -800., 800., 400, -800., 800.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/ch1/hdDecayedPionMuonXYcond",
	    "Y vs X ch0 < 20mm, ch1 < 50mm", 400, -100., 100., 400, -100., 100.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/ch1/hPosPiXY", "Y vs X", 300, -1000., 1000.,
	    300, -1000., 1000.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/ch1/hNegPiXY", "Y vs X", 300, -1000., 1000.,
	    300, -1000., 1000.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/ch1/hPosMuXY", "Y vs X", 300, -1000., 1000.,
	    300, -1000., 1000.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/ch2/hPosPiXY", "Y vs X", 300, -1000., 1000.,
	    300, -1000., 1000.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/ch2/hNegPiXY", "Y vs X", 300, -1000., 1000.,
	    300, -1000., 1000.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/ch2/hPosMuXY", "Y vs X", 300, -1000., 1000.,
	    300, -1000., 1000.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/ch3/hPosPiXY", "Y vs X", 300, -1000., 1000.,
	    300, -1000., 1000.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/ch3/hNegPiXY", "Y vs X", 300, -1000., 1000.,
	    300, -1000., 1000.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/ch3/hPosMuXY", "Y vs X", 300, -1000., 1000.,
	    300, -1000., 1000.));

	  /* Histos for Kine parts */
	  // No pion decayed to muon neutrino
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hChosenMM2PiPi0", 
	    "Chosen vertex Missing mass squared (K-#pi^{+}-#pi^{-})^{2}; [GeV^{2}/c^{4}]",
	    300, -0.05, 0.05));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hPosPiTotalQuality0",
	    "#pi^{+} Total Quality", 300, 0., 5.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hNegPiTotalQuality0",
	    "#pi^{-} Total Quality", 300, 0., 5.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hPosMuTotalQuality0",
	    "#mu^{+} Total Quality", 300, 0., 5.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hPosPiHoughQuality0",
	    "#pi^{+} Hough Quality", 300, 0., 5.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hNegPiHoughQuality0",
	    "#pi^{-} Hough Quality", 300, 0., 5.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hPosMuHoughQuality0",
	    "#mu^{+} Hough Quality", 300, 0., 5.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hMuonChi20",
	    "#mu^{+}  Chi2", 300, 0., 20.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hMuonYSlopeDiff0",
	    "#mu^{+} (#theta_{Y,bm} - #theta_{Y,am})", 400, -0.001, 0.001));
	  // One pion decayed to muon and neutrino
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hTrueVtxZPionVsMM2PiPi",
            "True Vtx Z position #pi vs (p_{K^{+}}-p_{#pi^{+}}-p_{#pi^{-}})^{2}", 300, -0.05, 0.05,
	    400, 105000, 270000));
  	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hTrueVtxZPionVsMM2PiPiMu",
            "True Vtx Z position #pi vs (p_{K^{+}}-p_{#pi^{+}}-p_{#pi^{-}}-p_{#mu^{+}})^{2}", 300, -0.001, 0.001,
	    400, 105000, 270000));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hTrueVtxZPionVsDiffPionKaonVtxZ",
	    "True Vtx Z position #pi vs Vtx (z(#pi) - z(K))", 400, 0, 50000,
	    400, 105000, 270000));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hTrueVtxZPionVsVtxZKaon",
	    "Vtx Z position K vs Vtx Z position #pi^{+}", 400, 105000, 190000,
	    400, 105000, 270000));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hTrueVtxZPionVsPionRFMuonP",
	    "True Vtx Z position #pi^{+} vs |#vec{p}_{#mu}|; [MeV/c]", 300, 0, 100.,
	    400, 105000, 270000));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hDiffPionKaonVtxZVsPionRFMuonP",
	    "Vtx (z(#pi) - z(K)) vs |#vec{p}_{#mu}|; [MeV/c]", 300, 0, 100., 400, 0, 300000));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hTrueVtxZPionVsMuonYSlopeDiff",
	    "True #pi^{+} Vtx Z position vs #mu^{+} (#theta_{Y,bm} - #theta_{Y,am})",
	    400, -0.001, 0.001, 400, 105000, 270000));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hPosPionDecayedPVsUndPionP",
	    "|#vec{p_{#pi^{+}, dec}}| vs |#vec{p_{#pi^{+}, und}}|; p [MeV/c]", 300, 0., 100000., 300, 0., 100000));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hNegPionDecayed",
	    "|#vec{p_{#pi^{-}, true}}|; p [MeV/c]", 300, 0., 100000.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hdPosPion",
	    "|#vec{p_{#pi^{+}}} - #vec{p_{#pi^{+}, true}}|; p [MeV/c]", 300, 0., 1000.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hdNegPion",
	    "|#vec{p_{#pi^{-}}} - #vec{p_{#pi^{-}, true}}|; p [MeV/c]", 300, 0., 1000.));
	  BookHisto(new TH1F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hdPosMuon",
	    "|#vec{p_{#mu^{+}}} - #vec{p_{#mu^{+}, true}}|; p [MeV/c]", 300, 0., 1000.));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hTrueVtxZPionVsGTKMuonCDA",
	    "True #pi^{+} Vtx Z position vs CDA GTK and #mu", 300, 0., 40., 400, 1050000., 270000.));

	  // Two pions decayed to muon and neutrino
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hTrueVtxZPion1VsPionRFMuonP",
	    "True Vtx Z position #pi^{+} vs |#vec{p}_{#mu}|; [MeV/c]", 300, 0, 100.,
	    400, 105000, 270000));
	  BookHisto(new TH2F(PIDSwap[n] + fmask[k] + fcut[j][i] + "/KP/hTrueVtxZPion2VsPionRFMuonP",
	    "True Vtx Z position #pi^{+} vs |#vec{p}_{#mu}|; [MeV/c]", 300, 0, 100.,
	    400, 105000, 270000));
	}
      }
    }
  }
}


void Kmu4StrictSelection::StartOfRunUser() {
  if(!GetIsTree())
    return;
  if(!GetWithEventHeader())
    return;
  FillHisto("hNRuns", 0.5);
  fIDM5 = TriggerConditions::GetInstance()->GetL0TriggerID("RICH-QX");
  fIDM3 = TriggerConditions::GetInstance()->GetL0TriggerID("RICH-QX-MO1-LKr10");
  //std::cout << "Vstupnyfile" << GetCurrentFile()->GetName() << '\n';


  TFile *file_w = TFile::Open("Weights.root");

  if (!file_w || file_w->IsZombie()) {
    std::cerr << "Error: Cannot open file!" << std::endl;
    return;
  }

  hdGTK.reset((TH3D*)file_w->Get("dGTK"));
  gProbLKr10.reset((TGraphAsymmErrors*)file_w->Get("LKr10Prob"));
  hLKrPosPionClustersEnergyVsMomentum.reset((TH2D*)file_w->Get("hLKrPosPionClustersEnergyVsMomt"));
  hLKrNegPionClustersEnergyVsMomentum.reset((TH2D*)file_w->Get("hLKrNegPionClustersEnergyVsMomt"));
  hLKrPosMuonClustersEnergyVsMomentum.reset((TH2D*)file_w->Get("hLKrPosMuonClustersEnergyVsMomt"));
}

void Kmu4StrictSelection::StartOfBurstUser() {
  if(!GetIsTree())
    return;
  if(!GetWithEventHeader())
    return;
  FillHisto("hNBursts", 0.5);
}

void Kmu4StrictSelection::Process(Int_t) {
  if(!GetWithEventHeader())
    return;

  if(GetWithMC()) {
    Bool_t EmulatedRICH(false), EmulatedQX(false), EmulatedMO1(false);
    Bool_t EmulatedKTAG_HLT(false);
    Bool_t EmulatedSTRAW_HLT_M3(false), EmulatedSTRAW_HLT_M5(false);

    Int_t RICHtime = fPrimitiveHandler->GetTriggerTime(NA62::Trigger::kL0RICH);
    // x- run number 7000-9600 fill with nof selected km4 events

    EmulatedRICH = fPrimitiveHandler->CheckPrimitives("RICH", RICHtime);
    if(EmulatedRICH)
      FillHisto("hEmulatedRICH", 0.5);
    EmulatedQX   = fPrimitiveHandler->CheckPrimitives("QX", RICHtime);
    if(EmulatedQX)
      FillHisto("hEmulatedQX", 0.5);
    EmulatedMO1  = fPrimitiveHandler->CheckPrimitives("MO1", RICHtime);
    if(EmulatedMO1)
      FillHisto("hEmulatedMO1", 0.5);
    fEmulatedLKr10  = fPrimitiveHandler->CheckPrimitives("E10", RICHtime);
    if(fEmulatedLKr10)
      FillHisto("hEmulatedLKr10", 0.5);

    HLTEvent *HLTevent = GetHLTData();

    Bool_t KTAGok  = HLTevent->IsL1KTAGProcessed() && !HLTevent->IsL1KTAGEmptyPacket() &&
      !HLTevent->IsL1KTAGBadData();
    Bool_t STRAWok = HLTevent->IsL1StrawProcessed() && !HLTevent->IsL1StrawEmptyPacket() &&
      !HLTevent->IsL1StrawBadData();

    //EmulatedSTRAW_HLT_M3 = ((HLTevent->GetSTRAWResponse() & 0x3) >> 2) == 1;
    //EmulatedSTRAW_HLT_M5 = ((HLTevent->GetSTRAWResponse() & 0x2) >> 1) == 1;

    EmulatedSTRAW_HLT_M3 = HLTevent->GetSTRAWBitOn(2);
    if(EmulatedSTRAW_HLT_M3)
      FillHisto("hEmulatedSTRAWM3", 0.5);
    EmulatedSTRAW_HLT_M5 = HLTevent->GetSTRAWBitOn(1);
    if(EmulatedSTRAW_HLT_M5)
      FillHisto("hEmulatedSTRAWM5", 0.5);
    EmulatedKTAG_HLT = HLTevent->GetKTAGResponse() == 1;

    fIsL0M5 = EmulatedRICH && EmulatedQX; // MASK 5
    fIsL1M5 = EmulatedKTAG_HLT && EmulatedSTRAW_HLT_M5;
    
    fIsL0M3 = EmulatedRICH && EmulatedQX && EmulatedMO1 && fEmulatedLKr10; // MASK 3

    if(IsAnalyzerInHistory("FastMCHandler"))
      fIsL0M3 = EmulatedRICH && EmulatedQX && EmulatedMO1; // MASK 3
    
    fIsL1M3 = EmulatedKTAG_HLT && EmulatedSTRAW_HLT_M3;

    if(!KTAGok || !STRAWok) {
      return;
    }
  }
  else {
    fIsL0M5 = TriggerConditions::GetInstance()->L0TriggerOn(GetRunID(), GetL0Data(), fIDM5);
    fIsL1M5 = TriggerConditions::GetInstance()->L1TriggerOnIgnoringFlagging(GetRunID(), GetL1Data(),
									    fIDM5);

    fIsL0M3 = TriggerConditions::GetInstance()->L0TriggerOn(GetRunID(), GetL0Data(), fIDM3);
    fIsL1M3 = TriggerConditions::GetInstance()->L1TriggerOnIgnoringFlagging(GetRunID(), GetL1Data(),
									    fIDM3);
  }
  //Double_t Timestamp = GetEventHeader()->GetTimeStamp() * ClockPeriod / 1.e9;

  fEventSelected = false;
  SetOutputState("EventSelected", kOValid);
  SetOutputState("VertexIndex", kOValid);
  SetOutputState("KaonMass", kOValid);
  SetOutputState("KaonMomt", kOValid);
  SetOutputState("VertexPos", kOValid);
  SetOutputState("VertexTime", kOValid);

  fBeamThreeMomt = BeamParameters::GetInstance()->GetBeamThreeMomentum();
  fTriggerTime = GetL0Data()->GetReferenceFineTime() * TriggerCalib;
  fNSTRAWTracks = GetEvent<TRecoSpectrometerEvent>()->GetNCandidates();
  FillHisto("hNTracks", fNSTRAWTracks);

  // external tools outputs
  fDownTrack = *(std::vector<DownstreamTrack> *)GetOutput("DownstreamTrackBuilder.Output");
  fVertices =
    *(std::vector<SpectrometerTrackVertex> *)GetOutput("SpectrometerVertexBuilder.Output3");
  fVertices2tr =
    *(std::vector<SpectrometerTrackVertex> *)GetOutput("SpectrometerVertexBuilder.Output2");

  if(GetWithMC()) {
    Event *evt = GetMCEvent();
    EventBoundary *evt0 = static_cast<EventBoundary *>(evt->GetEventBoundary(0));
    fTrueKaonP = static_cast<KinePart*>(evt->GetKineParts()->At(0))->GetFinal4Momentum();

    if(evt0->GetNKineParts())
      FillHisto("hTrueVtxZ", evt->GetKinePart(0)->GetEndPos().Z());
  }

  if(!ProcessEvent())
    return;

  // Chosen vertices histogram
  SpectrometerTrackVertex vtx = fVertices[fChosenVtxIndex];
  Double_t chi2 = vtx.GetChi2();
  FillHisto(fPIDSwap + "/hChosenVtxChi2", chi2);

  fmomt = vtx.GetTotalMomentum();
  fbeam = fChosenBeamThreeMomt.Mag();
  fvpt = vtx.GetTotalThreeMomentum().Pt(fChosenBeamThreeMomt) / 1e3;
  fvtxZ = vtx.GetPosition().z();

  fKaonP.SetVectM(fChosenBeamThreeMomt, MKCH);
  TRecoGigaTrackerCandidate *gtk_c = 
    static_cast<TRecoGigaTrackerCandidate *>(GetEvent<TRecoGigaTrackerEvent>()->GetCandidate(
    fGTKCandIndex));
  TVector3 gtk_momt = gtk_c->GetMomentum();
  fKaonP_nosmear.SetVectM(gtk_momt, MKCH);
  Double_t ZMUV3 = GeometricAcceptance::GetInstance()->GetZMUV3();
  Double_t ZLKr = GeometricAcceptance::GetInstance()->GetZLKr();
  Double_t ZStraw[4] = {GeometricAcceptance::GetInstance()->GetZStraw(0),
    GeometricAcceptance::GetInstance()->GetZStraw(1),
    GeometricAcceptance::GetInstance()->GetZStraw(2),
    GeometricAcceptance::GetInstance()->GetZStraw(3)};

  fPosMuXY.clear(); fPosPiXY.clear(); fNegPiXY.clear();
  Int_t nofpospions = 0;

  for(Int_t i = 0; i < 3; i++) {
    TVector3 trackP = vtx.GetTrackThreeMomentum(i);
    TRecoSpectrometerCandidate *STRAWCand = fDownTrack[fTrackID.at(i)].GetSpectrometerCandidate();
    Double_t EoP = fDownTrack[fChosenTrackID.at(i)].GetLKrEoP();
    Double_t xAtMUV3FrontPlane = fDownTrack[fChosenTrackID.at(i)].xAt(ZMUV3);
    Double_t yAtMUV3FrontPlane = fDownTrack[fChosenTrackID.at(i)].yAt(ZMUV3);
    Double_t xAtLKrFrontPlane = fDownTrack[fChosenTrackID.at(i)].xAt(ZLKr);
    Double_t yAtLKrFrontPlane = fDownTrack[fChosenTrackID.at(i)].yAt(ZLKr);

    if(fChosenTrackID.at(i) == fPosMuonTrackID) {
      FillHisto(fPIDSwap + "/hChosenPosMuonEoP", EoP);
      FillHisto(fPIDSwap + "/hChosenXYPosMuonZMUV3", xAtMUV3FrontPlane, yAtMUV3FrontPlane);
      FillHisto(fPIDSwap + "/hChosenXYPosMuonZLKr", xAtLKrFrontPlane, yAtLKrFrontPlane);
      fPosMuTotalQuality = STRAWCand->GetCombinationTotalQuality();
      fPosMuHoughQuality = STRAWCand->GetCombinationHoughQuality();
      fPosMudSlope = STRAWCand->GetSlopeYBeforeMagnet() - STRAWCand->GetSlopeYAfterMagnet();
      for(Int_t j = 0; j < 4; j++)
        fPosMuXY.push_back({STRAWCand->xAt(ZStraw[j]), STRAWCand->yAt(ZStraw[j])});
      fPosMuonP_fmID.SetVectM(trackP, MPI);
    }
    else {
      if(vtx.GetTrackCharge(i) == 1 && nofpospions == 0) {
        FillHisto(fPIDSwap + "/hChosenPosPionEoP", EoP);
        FillHisto(fPIDSwap + "/hChosenXYPosPionZMUV3", xAtMUV3FrontPlane, yAtMUV3FrontPlane);
	FillHisto(fPIDSwap + "/hChosenXYPosPionZLKr", xAtLKrFrontPlane,  yAtLKrFrontPlane);
        fPosPiTotalQuality = STRAWCand->GetCombinationTotalQuality();
        fPosPiHoughQuality = STRAWCand->GetCombinationHoughQuality();
        for(Int_t j = 0; j < 4; j++)
          fPosPiXY.push_back({STRAWCand->xAt(ZStraw[j]), STRAWCand->yAt(ZStraw[j])});
	nofpospions++;
      }
      else {
        FillHisto(fPIDSwap + "/hChosenNegPionEoP", EoP);
        FillHisto(fPIDSwap + "/hChosenXYNegPionZMUV3", xAtMUV3FrontPlane, yAtMUV3FrontPlane);
        FillHisto(fPIDSwap + "/hChosenXYNegPionZLKr", xAtLKrFrontPlane, yAtLKrFrontPlane);
        fNegPiTotalQuality = STRAWCand->GetCombinationTotalQuality();
        fNegPiHoughQuality = STRAWCand->GetCombinationHoughQuality();
        for(Int_t j = 0; j < 4; j++)
          fNegPiXY.push_back({STRAWCand->xAt(ZStraw[j]), STRAWCand->yAt(ZStraw[j])});
      }
    }
  }

  // LKr Energy study  
  TRecoLKrEvent *LKRevent = GetEvent<TRecoLKrEvent>();
  fTotalClusterEnergy = 0.0;  // [MeV]
  fNClusters = LKRevent->GetNCandidates();
  fTotalNotMatchedClusterEnergy = 0.;
  fInTimeNotMatchedNClusters = 0;
  fNInTimeClusters = 0;
  fTotalClusterEnergy += fPosPionEnergy + fNegPionEnergy + fPosMuonEnergy;

  if(IsAnalyzerInHistory("FastMCHandler")) {
    fWeight = gProbLKr10->Eval(fTotalClusterEnergy, 0, "");
  }
  else
    fWeight = 1.;

  if(GetWithMC()) {
    TRandom3 randomGen;
    Double_t KaonP_x = randomGen.Gaus(fTrueKaonP.X(), sqrt(1.6898));
    Double_t KaonP_y = randomGen.Gaus(fTrueKaonP.Y(), sqrt(1.6708));
    Double_t KaonP_z = randomGen.Gaus(fTrueKaonP.Z(), sqrt(44748.));
    Int_t binIndex = hdGTK->FindBin(fGTKMomt.X() - KaonP_x, fGTKMomt.Y() - KaonP_y,
      fGTKMomt.Z() - KaonP_z);
    fdGTK_w = hdGTK->GetBinContent(binIndex) > 0. && hdGTK->GetBinContent(binIndex) < 1.5 ?
      hdGTK->GetBinContent(binIndex) : 1.;
  }

  Int_t PosPionNClusters = fDownTrack[fPosPionTrackID].GetLKrNAssociatedClusters();
  Int_t NegPionNClusters = fDownTrack[fNegPionTrackID].GetLKrNAssociatedClusters();
  Int_t PosMuonNClusters = fDownTrack[fPosMuonTrackID].GetLKrNAssociatedClusters();

  for(Int_t i = 0; i < fNClusters; i++) {
    TRecoLKrCandidate *Lcand = static_cast<TRecoLKrCandidate *>(LKRevent->GetCandidate(i));
    FillHisto(fPIDSwap + "/LKr/hLKrClusterTime", Lcand->GetTime() - fVertexTime);
    Bool_t matched = false;

    if(abs(Lcand->GetTime() - fVertexTime) < 10.) {
      for(Int_t j = 0; j < PosPionNClusters; j++) { // cluster Lcand must be matched with at least one tracks
        if(fDownTrack[fPosPionTrackID].GetLKrCandidate(j)->GetSeedID() == Lcand->GetSeedID())
	  matched = true;
      }
      for(Int_t j = 0; j < NegPionNClusters; j++) {
        if(fDownTrack[fNegPionTrackID].GetLKrCandidate(j)->GetSeedID() == Lcand->GetSeedID())
	  matched = true;
      }
      for(Int_t j = 0; j < PosMuonNClusters; j++) {
        if(fDownTrack[fPosMuonTrackID].GetLKrCandidate(j)->GetSeedID() == Lcand->GetSeedID())
	  matched = true;
      }
      if(!matched) {
        fTotalNotMatchedClusterEnergy += Lcand->GetEnergy();
	fInTimeNotMatchedNClusters++;
      }
      fNInTimeClusters++;
    }
  }
    
  FillHisto(fPIDSwap + "/LKr/hLKrClusterEnergy", fTotalClusterEnergy);
  Int_t RICHtime = fPrimitiveHandler->GetTriggerTime(NA62::Trigger::kL0RICH);

  if(fPrimitiveHandler->CheckPrimitives("E10", RICHtime))
    FillHisto(fPIDSwap + "/LKr/hLKrClusterEnergyLKr10", fTotalClusterEnergy);
  if(TriggerM3Condition())
    FillHisto(fPIDSwap + "/LKr/hLKrClusterEnergyM3", fTotalClusterEnergy);
  if(fEmulatedLKr10) {
    FillHisto(fPIDSwap + "/LKr/hLKr10VsLKrClusterEnergy", fTotalClusterEnergy, 1.5);
    FillHisto(fPIDSwap + "/LKr/hLKrClusterEnergyTrue", fTotalClusterEnergy);
  }
  else {
    FillHisto(fPIDSwap + "/LKr/hLKr10VsLKrClusterEnergy", fTotalClusterEnergy, 0.5);
  }
  FillHisto(fPIDSwap + "/LKr/hLKrClusterEnergyAll", fTotalClusterEnergy);

  if(TriggerM5Condition()) {
    FillHisto(fPIDSwap + "/LKr/hLKrClusterEnergyM5", fTotalClusterEnergy);
    if(fPrimitiveHandler->CheckPrimitives("E10", RICHtime))
      FillHisto(fPIDSwap + "/LKr/hLKrClusterEnergyM5LKr10", fTotalClusterEnergy);
  }

  fPosMuonP3 = fPosMuonP.P();
  fPosPionP3 = fPosPionP.P();
  fNegPionP3 = fNegPionP.P();
  fMMissSq = (fKaonP - fPosPionP - fNegPionP - fPosMuonP).M2() / 1e6;
  fMMissSq_nosmear = (fKaonP_nosmear - fPosPionP - fNegPionP - fPosMuonP).M2() / 1e6;
  fMMissSq_2pi = (fKaonP - fPosPionP - fNegPionP).M2() / 1e6;
  fMMissSq_mu = (fKaonP - fPosMuonP).M2() / 1e6;
  fMMissSq_fmID = (fKaonP - fPosPionP - fNegPionP - fPosMuonP_fmID).M2() / 1e6;
  fNumomt = (fKaonP - fPosPionP - fNegPionP - fPosMuonP).Vect().Mag() / 1e3;
  TLorentzVector PosPionP_fmID;
  PosPionP_fmID.SetVectM(fPosPionP.Vect(), MMU);
  ftlcda->SetLine2PointDir(fChosenMuonTrack.GetPositionBeforeMagnet(),
  	  		   fChosenMuonTrack.GetMomentumBeforeMagnet());
  ftlcda->ComputeVertexCDA();
  fCDAGTKMuon = ftlcda->GetCDA();

  // PLD Kaon+Pion+Pion and Muon
  fVertexLSF.Reset();
  fVertexLSF.AddTrack(fDownTrack[fPosPionTrackID].GetSpectrometerCandidate());
  fVertexLSF.AddTrack(fDownTrack[fNegPionTrackID].GetSpectrometerCandidate());
  TRecoGigaTrackerCandidate *gtkcand = 
    static_cast<TRecoGigaTrackerCandidate *>(GetEvent<TRecoGigaTrackerEvent>()->GetCandidate(
    fGTKCandIndex));
  fVertexLSF.AddGTKTrack(gtkcand);
  fVertexLSF.FitVertex(true); // blue field correction
  TVector3 GTKPiPivtxpos = fVertexLSF.GetVertexPosition();
  fPLD->SetPoint(GTKPiPivtxpos);
  fPLD->SetLinePoint1(fDownTrack[fPosMuonTrackID].GetPositionBeforeMagnet());
  fPLD->SetLineDir(fDownTrack[fPosMuonTrackID].GetMomentumBeforeMagnet());
  fPLD->ComputeDistance();
  fPLDMuonGTK2Pi = fPLD->GetDistance();

  TVector3 boostvector = fKaonP.BoostVector();
  TLorentzVector KaonP_KaonRF = fKaonP;
  KaonP_KaonRF.Boost(-boostvector);
  TLorentzVector PosMuonP_KaonRF = fPosMuonP;
  PosMuonP_KaonRF.Boost(-boostvector);
  TLorentzVector PosPionP_KaonRF = fPosPionP;
  PosPionP_KaonRF.Boost(-boostvector);
  TLorentzVector NegPionP_KaonRF = fNegPionP;
  NegPionP_KaonRF.Boost(-boostvector);
  TLorentzVector DecayedPionP_KaonRF = KaonP_KaonRF - PosPionP_KaonRF - NegPionP_KaonRF;

  TLorentzVector DecayedPionP_PionRF = fKaonP - fPosPionP - fNegPionP;
  TVector3 boostvector_PionRF = DecayedPionP_PionRF.BoostVector();
  TLorentzVector PosMuonP_PionRF = fPosMuonP;
  PosMuonP_PionRF.Boost(-boostvector_PionRF);
  TLorentzVector PosMisIDMuonP_PionRF;
  PosMisIDMuonP_PionRF.SetVectM(fPosMuonP.Vect(), MPI);
  PosMisIDMuonP_PionRF.Boost(-boostvector_PionRF);
  DecayedPionP_PionRF.Boost(-boostvector_PionRF);

  fcost = (PosMuonP_PionRF.Vect()).Dot(DecayedPionP_KaonRF.Vect()) / (PosMuonP_PionRF.Vect().Mag() *
    DecayedPionP_KaonRF.Vect().Mag());

  TLorentzVector DecayedPionP = fKaonP - fPosPionP - fNegPionP;
  fMuonPionRF3P_mag = PosMuonP_PionRF.Vect().Mag();

  // Decayed Pion XY extrapolation from GTK + Pi + Pi vertex
  Double_t thx = DecayedPionP.X() / DecayedPionP.Z();
  Double_t thy = DecayedPionP.Y() / DecayedPionP.Z();
  fDecayedPionXYch0[0] = GTKPiPivtxpos.X() + thx *
    (GeometricAcceptance::GetInstance()->GetZStraw(0) - GTKPiPivtxpos.Z());
  fDecayedPionXYch0[1] = GTKPiPivtxpos.Y() + thy *
    (GeometricAcceptance::GetInstance()->GetZStraw(0) - GTKPiPivtxpos.Z());
  fDecayedPionXYch1[0] = GTKPiPivtxpos.X() + thx *
    (GeometricAcceptance::GetInstance()->GetZStraw(1) - GTKPiPivtxpos.Z());
  fDecayedPionXYch1[1] = GTKPiPivtxpos.Y() + thy *
    (GeometricAcceptance::GetInstance()->GetZStraw(1) - GTKPiPivtxpos.Z());

  fPosMuonP3_RF = PosMuonP_KaonRF.Vect().Mag();
  TLorentzVector NuP_KaonRF = (KaonP_KaonRF - PosPionP_KaonRF - NegPionP_KaonRF - PosMuonP_KaonRF);
  fThetaPosPiPosMu = cos(PosPionP_KaonRF.Angle(PosMuonP_KaonRF.Vect()));
  fThetaNegPiPosMu = cos(NegPionP_KaonRF.Angle(PosMuonP_KaonRF.Vect()));
  fThetaPosMuNu = cos(PosMuonP_KaonRF.Angle(NuP_KaonRF.Vect()));
  fThetaPosPiNu = cos(PosPionP_KaonRF.Angle(NuP_KaonRF.Vect()));
  fThetaNegPiNu = cos(NegPionP_KaonRF.Angle(NuP_KaonRF.Vect()));
  fThetaPosMuDecayedPi = cos(PosMuonP_KaonRF.Angle(DecayedPionP_KaonRF.Vect()));
  fThetaPosPiDecayedPi = cos(PosPionP_KaonRF.Angle(DecayedPionP_KaonRF.Vect()));
  fThetaNegPiDecayedPi = cos(NegPionP_KaonRF.Angle(DecayedPionP_KaonRF.Vect()));

  /*if(TriggerM5Condition())
    CutApply(fmask[0]);*/

  if(TriggerM3Condition()) {
    CutApply(fmask[0]);
  }

  /*if(TriggerM5Condition() || TriggerM3Condition())
    CutApply(fmask[2]);*/
}

void Kmu4StrictSelection::PostProcess() {
}

void Kmu4StrictSelection::EndOfBurstUser() {
}

void Kmu4StrictSelection::EndOfRunUser() {
}

void Kmu4StrictSelection::EndOfJobUser() {
  fMatchGTK->SaveAllPlots();
  SaveAllPlots();
}

Bool_t Kmu4StrictSelection::TriggerM5Condition() {
  return fIsL0M5 && fIsL1M5;
}

Bool_t Kmu4StrictSelection::TriggerM3Condition() {
  return fIsL0M3 && fIsL1M3;
}

Bool_t Kmu4StrictSelection::tracksPassCuts() {
  Bool_t InAcceptance = true;
  for(Int_t i = 0; i < 3; i++) {
    DownstreamTrack trck = fDownTrack[fTrackID.at(i)];
    if(!GeometricAcceptance::GetInstance()->InAcceptance(&trck, NA62::kSpectrometer, 0)) {
      InAcceptance = false;
    }
    if(!GeometricAcceptance::GetInstance()->InAcceptance(&trck, NA62::kSpectrometer, 1)) {
      InAcceptance = false;
    }
    if(!GeometricAcceptance::GetInstance()->InAcceptance(&trck, NA62::kSpectrometer, 2)) {
      InAcceptance = false;
    }
    if(!GeometricAcceptance::GetInstance()->InAcceptance(&trck, NA62::kSpectrometer, 3)) {
      InAcceptance = false;
    }
    if(!GeometricAcceptance::GetInstance()->InAcceptance(&trck, NA62::kNewCHOD)) {
      InAcceptance = false;
    }
    if(!GeometricAcceptance::GetInstance()->InAcceptance(&trck, NA62::kCHOD)) {
      InAcceptance = false;
    }
    if(!GeometricAcceptance::GetInstance()->InAcceptance(&trck, NA62::kLKr)) {
      InAcceptance = false;
    }
    if(!GeometricAcceptance::GetInstance()->InAcceptance(&trck, NA62::kMUV3)) {
      InAcceptance = false;
    }
  }
  if(!InAcceptance)
    return false;

  fVertexTime = 0.;

  Bool_t VertexTriggerInTime(false);
  Bool_t AllTracksGoodEoP(true);
  Bool_t AllTracksGoodCh2(true);
  Bool_t AllTracksGoodMom(true);

  for(UInt_t j = 0; j < 3; j++) {
    Int_t TID = fTrackID.at(j);

    Double_t Ch2 = fDownTrack[TID].GetChi2();
    FillHisto(fPIDSwap + "/hTrackChi2", Ch2);
    if(Ch2 > fCutTrackChi2) {
      AllTracksGoodCh2 = false;
    }

    Double_t Mom = fDownTrack[TID].GetMomentum();
    FillHisto(fPIDSwap + "/hTrackMomt", Mom);
    if(Mom < fCutTrackLowMomt || Mom > fCutTrackHighMomt) {
      AllTracksGoodMom = false;
    }

    Double_t BFAF = fabs(fDownTrack[TID].GetMomentum() - fDownTrack[TID].GetMomentumBeforeFit());
    if(BFAF > fCutTrackAfterBeforeFitMomDiff) {
      AllTracksGoodMom = false;
    }

    fTrackTime.at(j) = fVertices.at(fVtxIndex).GetTrackCHODTime(j);
  }
  if(!AllTracksGoodEoP)
    return false;
  if(!AllTracksGoodCh2)
    return false;
  if(!AllTracksGoodMom)
    return false;

  // vertex timing
  Int_t Nclose = 0;
  fVertexTime = fVertices.at(fVtxIndex).GetCHODTime();
  for(Int_t i = 0; i < 3; i++) {
    FillHisto(fPIDSwap + "/hTrackVertexTime", fTrackTime.at(i) - fVertexTime);
    if(fabs(fTrackTime.at(i) - fVertexTime) < fCutTrackVertexTime)
      Nclose++;
  }
  FillHisto(fPIDSwap + "/hNtracksInTimeWithVtx", Nclose);
  if(Nclose != 3) {
    return false;
  }

  FillHisto(fPIDSwap + "/hVertexTriggerTime", fVertexTime - fTriggerTime);
  if(fabs(fVertexTime - fTriggerTime) < fCutVertexTriggerTime) {
    VertexTriggerInTime = true;
  }
  if(!VertexTriggerInTime)
    return false;

  return true;
}

Bool_t Kmu4StrictSelection::ProcessEvent() {
  // get the number of good 3-track vertices in the event
  fNVertices = 0;
  fVtxIndex = -1;
  fChosenVtxIndex = -1;
  FillHisto("hVerticesSize", fVertices.size());
  
  for(UInt_t i = 0; i < fVertices.size(); i++) {
    TRecoGigaTrackerEvent *GTKevent = GetEvent<TRecoGigaTrackerEvent>();
    fMatchGTK->Process(fVertices[i], GTKevent);

    if(fMatchGTK->GetGTKMatchedSuccessfully())
      fGTKCandIndex = fMatchGTK->GetMatchedGTKTrackID();
    else
      continue;

    const SpectrometerTrackVertex& vtx = fMatchGTK->GetUpdatedSpectrometerTrackVertex();
    fVertices[i] = fMatchGTK->GetUpdatedSpectrometerTrackVertex();
    
    Double_t chrg = vtx.GetCharge();
    FillHisto("hVtxCharge", chrg);
    if(chrg != 1)
      continue;

    Double_t chi2 = vtx.GetChi2();
    FillHisto("hVtxChi2", chi2);
    if(chi2 > fCutVertexChi2)
      continue;

    Double_t vtxZ = vtx.GetPosition().z();
    FillHisto("hVtxZ", vtxZ);
    if(vtxZ < fCutZVertexMin || vtxZ > fCutZVertexMax)
      continue;
    fVtxIndex = i;

    Int_t nofpions = 0;
    Int_t nofmuons = 0;
    Int_t muonCh = 0;
    Int_t muonID = 0;

    for(UInt_t j = 0; j < 3; j++) {
      fTrackID.at(j) = fVertices[fVtxIndex].GetTrackIndex(j);
      fTrackCharge.at(j) = fDownTrack[fTrackID.at(j)].GetCharge();
      fTrackTime.at(j) = fDownTrack[fTrackID.at(j)].GetTrackTime();
      fTrackThreeMomt.at(j) = vtx.GetTrackThreeMomentum(j);
      Double_t E = fDownTrack[fTrackID.at(j)].GetLKrEnergy() / 1e3;
      Double_t P = fDownTrack[fTrackID.at(j)].GetMomentum() / 1e3;
      
      if(isPion(j)) {
	if(IsAnalyzerInHistory("FastMCHandler")) {
	  if(fTrackCharge.at(j) == 1) {
            Int_t binx = hLKrPosPionClustersEnergyVsMomentum->GetXaxis()->FindBin(P);
            std::unique_ptr<TH1D> hist1D(hLKrPosPionClustersEnergyVsMomentum->ProjectionY("projy", binx, binx));
            fPosPionEnergy = hist1D->GetRandom();

  	    if(fPosPionEnergy / P < fCutPionLKrEoP)
  	      nofpions++;
	  }
	  else {
            Int_t binx = hLKrNegPionClustersEnergyVsMomentum->GetXaxis()->FindBin(P);
            std::unique_ptr<TH1D> hist1D(hLKrNegPionClustersEnergyVsMomentum->ProjectionY("projy", binx, binx));
            fNegPionEnergy = hist1D->GetRandom();
	    
            if(fNegPionEnergy / P < fCutPionLKrEoP)
   	      nofpions++;
	  }
	}
	else {
  	  if(nofpions == 0)
            fPosPionEnergy = E;
	  else
            fNegPionEnergy = E;
  	  nofpions++;
	}
      }
      else if(isMuon(j)) {
        if(IsAnalyzerInHistory("FastMCHandler")) {	
          Int_t binx = hLKrPosMuonClustersEnergyVsMomentum->GetXaxis()->FindBin(P);
          std::unique_ptr<TH1D> hist1D(hLKrPosMuonClustersEnergyVsMomentum->ProjectionY("projy", binx, binx));
          fPosMuonEnergy = hist1D->GetRandom();

          if(fPosMuonEnergy / P < fCutMuonLKrEoP)
  	    nofmuons++;	
	}
	else {
          fPosMuonEnergy = E;
	  nofmuons++;
	}
        muonCh = fTrackCharge.at(j);
	muonID = fTrackID.at(j);
      }
    }
        
    if(nofpions != 2 || nofmuons != 1) {
      continue;
    }

    Int_t countpospions = 0;
    
    for(UInt_t j = 0; j < 3; j++) {
      if(muonCh == 1) {
        if(fTrackID.at(j) == muonID) {
	  fPosMuonP.SetVectM(fTrackThreeMomt.at(j), MMU);
          fPosMuonTrackID = fTrackID.at(j);
          fChosenMuonTrack = fDownTrack[fTrackID.at(j)];
	}
	else {
          if(fTrackCharge.at(j) == 1) {
	    fPosPionP.SetVectM(fTrackThreeMomt.at(j), MPI);
	    fPosPionTrackID = fTrackID.at(j);
	  }
	  else {
            fNegPionP.SetVectM(fTrackThreeMomt.at(j), MPI);
	    fNegPionTrackID = fTrackID.at(j);
	  }
	}
        fPIDSwap = "";
      }
      else {
        if(fTrackID.at(j) == muonID) {
	  fPosMuonP.SetVectM(fTrackThreeMomt.at(j), MMU);
          fPosMuonTrackID = fTrackID.at(j);
          fChosenMuonTrack = fDownTrack[fTrackID.at(j)];	  
	}
	else {
          if(countpospions == 0) {
	    fPosPionP.SetVectM(fTrackThreeMomt.at(j), MPI);
	    fPosPionTrackID = fTrackID.at(j);
	    countpospions++;
	  }
	  else {
	    fNegPionP.SetVectM(fTrackThreeMomt.at(j), MPI);
	    fNegPionTrackID = fTrackID.at(j);
	  }
	}	
        fPIDSwap = "PIDSwap";
      }
    }

    FillHisto(fPIDSwap + "/LKr/hLKrPosPionClustersEnergyVsMomt", fPosPionP.Vect().Mag() / 1e3, fPosPionEnergy);
    FillHisto(fPIDSwap + "/LKr/hLKrNegPionClustersEnergyVsMomt", fNegPionP.Vect().Mag() / 1e3, fNegPionEnergy);
    FillHisto(fPIDSwap + "/LKr/hLKrPosMuonClustersEnergyVsMomt", fPosMuonP.Vect().Mag() / 1e3, fPosMuonEnergy);

    Bool_t tracksOK = tracksPassCuts();
    Double_t ktagtime = KTAGTime();
    if(!tracksOK || ktagtime == -9999.)
      continue;

    TRecoGigaTrackerCandidate *gtk = 
      static_cast<TRecoGigaTrackerCandidate *>(GetEvent<TRecoGigaTrackerEvent>()->GetCandidate(
      fGTKCandIndex));

    ftlcda->SetLine1PointDir(gtk->GetPosition(NA62::kGTK3), gtk->GetMomentum());

    fGTKMomt = gtk->GetMomentum();
    if(GetWithMC()) {
      GTKSmearing(fGTKMomt);
    }

    // Track separations in STRAW1 and LKr planes
    Double_t zstraw1 = GeometricAcceptance::GetInstance()->GetZStraw(0);
    for(Int_t ii = 0; ii < 2; ii++) {
      TRecoSpectrometerCandidate *Scand = fVertices[fVtxIndex].GetSpectrometerCandidate(ii);
      Double_t x1s = Scand->xAt(zstraw1);  // Straw1
      Double_t y1s = Scand->yAt(zstraw1);
      Double_t x1c = Scand->xAt(241093.0);  // LKr
      Double_t y1c = Scand->yAt(241093.0);
      for(Int_t jj = ii + 1; jj < 3; jj++) {
        TRecoSpectrometerCandidate *Scand2 = fVertices[fVtxIndex].GetSpectrometerCandidate(jj);
        Double_t x2s = Scand2->xAt(zstraw1);  // Straw1
        Double_t y2s = Scand2->yAt(zstraw1);
        Double_t x2c = Scand2->xAt(241093.0);  // LKr
        Double_t y2c = Scand2->yAt(241093.0);
        Double_t rs = sqrt((x1s - x2s) * (x1s - x2s) + (y1s - y2s) * (y1s - y2s));  // Straw1
        Double_t rc = sqrt((x1c - x2c) * (x1c - x2c) + (y1c - y2c) * (y1c - y2c));  // LKr
        FillHisto(fPIDSwap + "/hDistanceStraw1", rs);
        FillHisto(fPIDSwap + "/hDistanceLKr", rc);
      }
    }

    if(fVertices[fVtxIndex].GetMinTrackSeparationAtZ(zstraw1) < fCutStraw1Separation)
      continue;  // Straw1
    if(fVertices[fVtxIndex].GetMinTrackSeparationAtZ(241093.0) < fCutLKrSeparation)
      continue;  // LKr

    // N hits per STRAW candidate
    Bool_t goodcand = true;
    for(Int_t j = 0; j < 3; j++) {
      TRecoSpectrometerCandidate *STRAWCand =
        fDownTrack[fTrackID.at(j)].GetSpectrometerCandidate();
      FillHisto(fPIDSwap + "/hNStrawHitsInCandidate", STRAWCand->GetNHits());
      if(STRAWCand->GetNChambers() != 4)
        goodcand = false;
      else
        FillHisto(fPIDSwap + "/hNStrawHitsIn4CHCandidate", STRAWCand->GetNHits());

      FillHisto(fPIDSwap + "/hLKrDDeadCell", fDownTrack[fTrackID.at(j)].GetLKrClusterDDeadCell());
      if(fDownTrack[fTrackID.at(j)].GetLKrClusterDDeadCell() < fCutLKrClusterDDeadCell)
        goodcand = false;
    }
    if(!goodcand)
      continue;

    // this is a good vertex, remember important variables
    fNVertices++;
    fChosenVtxIndex = fVtxIndex;
    fChosenVertexTime = fVertexTime;
    fChosenKTAGTime = ktagtime;
    fChosenBeamThreeMomt = fGTKMomt;

    for(UInt_t j = 0; j < 3; j++) {
      fChosenTrackID.at(j) = fTrackID.at(j);
      fChosenTrackTime.at(j) = fTrackTime.at(j);
    }
  }

  // require exactly one good 3-track vertex
  FillHisto(fPIDSwap + "/hNVertices", fNVertices);
  if(fNVertices != 1)
    return false;
  else
    return true;
}

Double_t Kmu4StrictSelection::KTAGTime() {
  TRecoCedarEvent *CedarEvent = GetEvent<TRecoCedarEvent>();
  Double_t KTAGTimetoVtx{ -9999. };
  if(CedarEvent->GetNCandidates() == 0)
    return KTAGTimetoVtx;

  for(Int_t iCand = 0; iCand < CedarEvent->GetNCandidates(); iCand++) {
    TRecoCedarCandidate *CedarCand =
      static_cast<TRecoCedarCandidate *>(CedarEvent->GetCandidate(iCand));
    if(CedarCand->GetNSectors() < 5) {
      continue;
    }
    Double_t KTAGTime = CedarCand->GetTime();
    FillHisto(fPIDSwap + "/hTriggerKTAGTime", fTriggerTime - KTAGTime);
    if(fabs(fTriggerTime - KTAGTime) > fCutTriggerKTAGTime) {
      continue;
    }
    FillHisto(fPIDSwap + "/hVertexKTAGTime", fVertexTime - KTAGTime);
    if(fabs(fVertexTime - KTAGTime) > fCutVertexKTAGTime) {
      continue;
    }
    if(fabs(fVertexTime - KTAGTime) < fabs(fVertexTime - KTAGTimetoVtx))
      KTAGTimetoVtx = KTAGTime;
  }
  return KTAGTimetoVtx;
}

Bool_t Kmu4StrictSelection::isMuon(UInt_t j) {
  DownstreamTrack trck = fDownTrack[fTrackID.at(j)];
  Double_t vertexTime = fVertices[fVtxIndex].GetCHODTime();
  Bool_t isMuon = fDownTrack[fTrackID.at(j)].MUV3AssociationExists() &&
    fDownTrack[fTrackID.at(j)].MUV3InTimeOuterAssociationExists(vertexTime, 
    fCutMuonVertexMUV3Time) && fDownTrack[fTrackID.at(j)].GetLKrEoP() < fCutMuonLKrEoP;
  //  && fDownTrack[fTrackID.at(j)].GetLKrEoP() < fCutMuonLKrEoP
  return isMuon;
}

Bool_t Kmu4StrictSelection::isPion(UInt_t j) {
  DownstreamTrack trck = fDownTrack[fTrackID.at(j)];
  Double_t vertexTime = fVertices[fVtxIndex].GetCHODTime();
  Bool_t isPion = !(fDownTrack[fTrackID.at(j)].MUV3InTimeAssociationExists(vertexTime,
    fCutPionVertexMUV3Time)) && fDownTrack[fTrackID.at(j)].GetLKrEoP() < fCutPionLKrEoP;
  //  && fDownTrack[fTrackID.at(j)].GetLKrEoP() < fCutPionLKrEoP
  return isPion;
}

void Kmu4StrictSelection::GTKSmearing(TVector3& vec) {
  Double_t x = vec.X();
  Double_t y = vec.Y();
  Double_t z = vec.Z();
  vec.SetXYZ(x + (1.045 - 1) * (x - fTrueKaonP.X()) + 0.4084, y + (1.095 - 1) * (y - fTrueKaonP.Y()),
    z + (1.436 - 1) * (z - fTrueKaonP.Z()) + 11.793);
}

Bool_t Kmu4StrictSelection::MatchedTracksTwoTracksVtx(SpectrometerTrackVertex vtx) {
  Int_t matchedTr = 0;
  for(UInt_t i = 0; i < 2; i++) {
    for(UInt_t j = 0; j < 3; j++) {
      if(vtx.GetTrackIndex(i) == fTrackID.at(j))
	matchedTr++;
    }
  }
  if(matchedTr == 2)
    return true;
  else
    return false;
}


void Kmu4StrictSelection::TwoTracksVtxHisto(TString mask, TString cut) {
  fChosen2trVtxIndex = 0;
  Int_t misGTKIndex = 0;
  Double_t maxdiffVtxZ = 0.;

  for(UInt_t i = 0; i < fVertices2tr.size(); i++) {
    SpectrometerTrackVertex vtx2tr = fVertices2tr[i];
    Int_t firstTrack = false;
    Int_t secondTrack = false;

    TRecoGigaTrackerEvent *GTKevent = GetEvent<TRecoGigaTrackerEvent>();
    fMatchGTK->Process(fVertices2tr[i], GTKevent);
    Double_t GTKCandIndex = fMatchGTK->GetMatchedGTKTrackID();

    if(fGTKCandIndex != GTKCandIndex && MatchedTracksTwoTracksVtx(fVertices2tr[i]))
      misGTKIndex++;
    
    if(i - 1 != fVertices2tr.size()) {
      for(UInt_t j = i + 1; j < fVertices2tr.size(); j++) {
	Double_t diffVtxZ = fVertices2tr[i].GetPosition().z() - fVertices2tr[j].GetPosition().z();
	if(MatchedTracksTwoTracksVtx(fVertices2tr[i]) && MatchedTracksTwoTracksVtx(fVertices2tr[j])) {
	  maxdiffVtxZ = (abs(diffVtxZ) > abs(maxdiffVtxZ)) ? diffVtxZ : maxdiffVtxZ;
	  FillHisto(mask + cut + "/h2TrVtxdZ", diffVtxZ);
	}
      }
    }

    for(Int_t k = 0; k < 2; k++) {
      if(vtx2tr.GetTrackIndex(k) == fPosPionTrackID)
	firstTrack = true;
      if(vtx2tr.GetTrackIndex(k) == fNegPionTrackID)
	secondTrack = true;
    }
    if(firstTrack && secondTrack) {
      fChosen2trVtxIndex = i;
      FillHisto(mask + cut + "/hChosenVtxTr2", 0.5);
    }
  }

  if(maxdiffVtxZ != 0.)
    FillHisto(mask + cut + "/h2TrMaxVtxdZ", maxdiffVtxZ);
  FillHisto(mask + cut + "/hMisGTKIndex", misGTKIndex);

  fPLD->SetPoint(fVertices2tr[fChosen2trVtxIndex].GetPosition());
  fPLD->SetLinePoint1(fDownTrack[fPosMuonTrackID].GetPositionBeforeMagnet());
  fPLD->SetLineDir(fDownTrack[fPosMuonTrackID].GetMomentumBeforeMagnet());
  fPLD->ComputeDistance();
  fPLDMuonPair = fPLD->GetDistance();
  FillHisto(mask + cut + "/hPLDDistanceMuonPair", fPLDMuonPair);
}

void Kmu4StrictSelection::TrueFillHistos(TString mask, TString cut) {
  if(GetWithMC()) {
    Event *evt = GetMCEvent();
    EventBoundary *evt0 = static_cast<EventBoundary *>(evt->GetEventBoundary(0));

    if(evt0->GetNKineParts()) {
      Int_t NPion = 0;
      Double_t trueVtxZPion = 0, trueVtxZ = 0, diffZ = 0;
      Double_t trueVtxZPion2 = 0;
      TVector3 PosPionDecP3(0, 0, 0), NegPionDecP3(0, 0, 0), PosPion2P3(0, 0, 0), NegPionP3, PosMuonP3, NeutrinoP3;

      for(Int_t i = 1; i < evt0->GetNKineParts(); i++) {
	KinePart* mcTrackp = static_cast<KinePart*>(evt->GetKineParts()->At(i - 1));
	KinePart* mcTrack = static_cast<KinePart*>(evt->GetKineParts()->At(i));

	/*if(mask == "mask3" && cut == "/AfterdMomtGTK") {
	  std::cout << "------------ " << i << " ----------------" << '\n';
	  std::cout << "Pt " << fvpt << " " << " m3pi - mK " << ((fPosPionP + fNegPionP +
	    fPosMuonP_fmID).M() - MKCH) / 1e3 << '\n';
          std::cout << "PosMu*PosPi " << fPosMuonP*fPosPionP << " PosMu*NegPi " << 
            fPosMuonP*fNegPionP << " PosPi*NegPi " << fPosPionP*fNegPionP << '\n';
	  std::cout << "GTK " <<  fGTKMomt.Px() << " " << fGTKMomt.Py() << " " <<
	    fGTKMomt.Pz() << '\n';
	  std::cout << std::setprecision(16);
	  std::cout << "PosMu " << fPosMuonP.E() << " " << fPosMuonP.Px() << " " << 
            fPosMuonP.Py() << " " << fPosMuonP.Pz() << '\n';
	  std::cout << "PosPi " << fPosPionP.E() << " " << fPosPionP.Px() << " " << 
            fPosPionP.Py() << " " << fPosPionP.Pz() << '\n';
	  std::cout << "NegPi " << fNegPionP.E() << " " << fNegPionP.Px() << " " << 
            fNegPionP.Py() << " " << fNegPionP.Pz() << '\n';
	  mcTrack->Print();
	  mcTrackp->Print();
	  }*/
        if(mcTrack->GetPDGcode() == 211)
	  if(NPion == 1 || PosPion2P3.Mag() == 0)
  	    PosPion2P3 = mcTrack->GetInitialMomentum();
        if(mcTrack->GetPDGcode() == -211)
	  NegPionP3 = mcTrack->GetInitialMomentum();
	if(mcTrack->GetPDGcode() == -13)
	  PosMuonP3 = mcTrack->GetInitialMomentum();
	if(mcTrack->GetPDGcode() == 14)
	  NeutrinoP3 = mcTrack->GetInitialMomentum();

	if(mcTrack->GetParentIndex() > 0 && abs(mcTrack->GetPDGcode()) == 13 &&
	  (mcTrackp->GetEndPos().Z() == mcTrack->GetProdPos().Z()) &&
	   mcTrackp->GetEndProcessName() == "Decay") {
          if(NPion == 0) {
	    trueVtxZPion = mcTrack->GetProdPos().Z();
	    trueVtxZ = evt->GetKinePart(0)->GetEndPos().Z();
	    diffZ = trueVtxZPion - trueVtxZ;
   	    if(mcTrackp->GetPDGcode() == 211)
     	      PosPionDecP3 = mcTrackp->GetInitialMomentum();
	    else if(mcTrackp->GetPDGcode() == -211)
       	      NegPionDecP3 = mcTrackp->GetInitialMomentum();
	  }

	  if(NPion == 1) {
	    trueVtxZPion2 = mcTrack->GetProdPos().Z();
	  }
  	  NPion++;
	}
      }

      if(NPion == 0) {
        FillHisto(fPIDSwap + mask + cut + "/KP/hChosenMM2PiPi0", fMMissSq_2pi);
	FillHisto(fPIDSwap + mask + cut + "/KP/hPosPiTotalQuality0", fPosPiTotalQuality);
	FillHisto(fPIDSwap + mask + cut + "/KP/hNegPiTotalQuality0", fNegPiTotalQuality);
	FillHisto(fPIDSwap + mask + cut + "/KP/hPosMuTotalQuality0", fPosMuTotalQuality);
	FillHisto(fPIDSwap + mask + cut + "/KP/hPosPiHoughQuality0", fPosPiHoughQuality);
	FillHisto(fPIDSwap + mask + cut + "/KP/hNegPiHoughQuality0", fNegPiHoughQuality);
	FillHisto(fPIDSwap + mask + cut + "/KP/hPosMuHoughQuality0", fPosMuHoughQuality);
	FillHisto(fPIDSwap + mask + cut + "/KP/hMuonYSlopeDiff0", fPosMudSlope);
      }
      
      else if(NPion == 1) {
	FillHisto(fPIDSwap + mask + cut + "/KP/hTrueVtxZPionVsMM2PiPi", fMMissSq_2pi, trueVtxZPion, fWeight*fdGTK_w);
	FillHisto(fPIDSwap + mask + cut + "/KP/hTrueVtxZPionVsMM2PiPiMu", fMMissSq, trueVtxZPion, fWeight*fdGTK_w);
	FillHisto(fPIDSwap + mask + cut + "/KP/hTrueVtxZPionVsDiffPionKaonVtxZ", diffZ, trueVtxZPion);
	FillHisto(fPIDSwap + mask + cut + "/KP/hTrueVtxZPionVsVtxZKaon", trueVtxZPion, trueVtxZ);
	FillHisto(fPIDSwap + mask + cut + "/KP/hTrueVtxZPionVsPionRFMuonP", fMuonPionRF3P_mag,
	  trueVtxZPion);
	FillHisto(fPIDSwap + mask + cut + "/KP/hDiffPionKaonVtxZVsPionRFMuonP", fMuonPionRF3P_mag, diffZ);
	FillHisto(fPIDSwap + mask + cut + "/KP/hTrueVtxZPionVsMuonYSlopeDiff", fPosMudSlope, trueVtxZPion);

        if(PosPionDecP3.Mag() != 0) {
    	  FillHisto(fPIDSwap + mask + cut + "/KP/hPosPionDecayedPVsUndPionP", PosPion2P3.Mag(), PosPionDecP3.Mag());
	}
	if(NegPionDecP3.Mag() != 0) {
	  FillHisto(fPIDSwap + mask + cut + "/KP/hNegPionDecayed", NegPionDecP3.Mag());
	}
	FillHisto(fPIDSwap + mask + cut + "/KP/hdPosPion", (PosPion2P3 - fPosPionP.Vect()).Mag());
	FillHisto(fPIDSwap + mask + cut + "/KP/hdNegPion", (NegPionP3 - fNegPionP.Vect()).Mag());
	FillHisto(fPIDSwap + mask + cut + "/KP/hdPosMuon", (PosMuonP3 - fPosMuonP.Vect()).Mag());
        FillHisto(fPIDSwap + mask + cut + "/KP/hTrueVtxZPionVsGTKMuonCDA", fCDAGTKMuon, trueVtxZPion);
      }
      
      else if(NPion == 2) {
	FillHisto(fPIDSwap + mask + cut + "/KP/hTrueVtxZPion1VsPionRFMuonP", fMuonPionRF3P_mag,
	  trueVtxZPion);
	FillHisto(fPIDSwap + mask + cut + "/KP/hTrueVtxZPion2VsPionRFMuonP", fMuonPionRF3P_mag,
	  trueVtxZPion2);
      }
    }
  }
}

void Kmu4StrictSelection::FillHistos(TString mask, TString cut) {
  for(Int_t l = 0; l < (abs(fMMissSq) < 0.001 ? 2 : 1); l++) {
    FillHisto(fPIDSwap + mask + cut + fsr[l] + "/hdGTKMomt", (fGTKMomt - fBeamThreeMomt).Mag() / 1e3, fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + fsr[l] + "/hMM2PiPiVsPionRFMuonP", fMuonPionRF3P_mag, fMMissSq_2pi, fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + fsr[l] + "/hChosenVtxPtVs3PiKaon", ((fPosPionP + fNegPionP +
      fPosMuonP_fmID).M() - MKCH) / 1e3, fvpt, fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + fsr[l] + "/hPosPiTotalQuality", fPosPiTotalQuality, fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + fsr[l] + "/hNegPiTotalQuality", fNegPiTotalQuality, fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + fsr[l] + "/hPosMuTotalQuality", fPosMuTotalQuality, fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + fsr[l] + "/hChosenMM2PiPi", fMMissSq_2pi, fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + fsr[l] + "/hChosenDiPionVsDiLepton", fMMissSq_2pi,
     (fPosPionP + fNegPionP).M2() / 1e6, fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + fsr[l] + "/hChosenCostVsMM2PiPiMu", fMMissSq, fcost, fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + fsr[l] + "/hChosenMM2PiPiMuVsPiPiMu_misID", fMMissSq, fMMissSq_fmID, fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + fsr[l] + "/hVtxPtVsNuMomt", fNumomt, fvpt, fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + fsr[l] + "/hChosenVtxPtVs3PiKaonVsNuMomt", ((fPosPionP + fNegPionP +
      fPosMuonP_fmID).M() - MKCH) / 1e3, fvpt, fNumomt, fWeight*fdGTK_w);
  }
  FillHisto(fPIDSwap + mask + cut + "/hVtxMomtVsPt", fvpt,
    (fVertices[fChosenVtxIndex].GetTotalThreeMomentum() - fChosenBeamThreeMomt).Mag() / 1e3, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hChosenPosMuonMomt", fPosMuonP3 / 1e3, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hChosenPosPionMomt", fPosPionP3 / 1e3, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hChosenNegPionMomt", fNegPionP3 / 1e3, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hChosenMM2PiPiMuVsPiPi", fMMissSq_2pi, fMMissSq, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hChosenMM2PiPiMu", fMMissSq, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hChosenMM2PiPiMu_now", fMMissSq_nosmear, fWeight);
  FillHisto(fPIDSwap + mask + cut + "/hChosenMM2PiPiMuVsMu", fMMissSq_mu, fMMissSq, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hChosenNuMomt", fNumomt, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hChosenVtxPt", fvpt, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hChosenVtxMomt", (fVertices[fChosenVtxIndex].GetTotalThreeMomentum() -
    fChosenBeamThreeMomt).Mag() / 1e3, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hChosenVtxZ", fvtxZ, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hChosenMM2PiPiMuVsNuMomt", fNumomt, fMMissSq, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hChosenNuMomtVsVtxZ", fvtxZ, fNumomt, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hChosenMM2PiPiMuVsVtxZ", fvtxZ, fMMissSq, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hChosenMM2PiPiVsPt", fvpt, fMMissSq_2pi, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hChosenMM2PiPiMuVsPt", fvpt, fMMissSq, fWeight*fdGTK_w);  
  FillHisto(fPIDSwap + mask + cut + "/hGTKMomt", fGTKMomt.Mag() / 1e3, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hTrueKaonMomt", fTrueKaonP.Vect().Mag() / 1e3, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hdGTKTrueKaonMomt", (fChosenBeamThreeMomt - fTrueKaonP.Vect()).Mag(), fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hGTKMuonCDA", fCDAGTKMuon, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hPLDMuonGTK2Pi", fPLDMuonGTK2Pi, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hPionRFMuonP", fMuonPionRF3P_mag, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hCostVsPionRFMuonP", fMuonPionRF3P_mag, fcost, fWeight*fdGTK_w);

  FillHisto(fPIDSwap + mask + cut + "/hNLKrClusters", fNClusters, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hNInTimeLKrClusters", fNInTimeClusters, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hTotalClusterEnergyVsNInTimeLKrClusters", fNInTimeClusters,
    fTotalClusterEnergy, fWeight*fdGTK_w);
  FillHisto(fPIDSwap + mask + cut + "/hNotMatchedClustersEnergyVsNotMatchedLKrClusters",
    fInTimeNotMatchedNClusters, 0.001 * fTotalNotMatchedClusterEnergy, fWeight*fdGTK_w);

  /* 2D histos for K3pi cut optimization */
  if(abs(fMMissSq) < 0.001) {
    FillHisto(fPIDSwap + mask + cut + "/hPosPiHoughQuality", fPosPiHoughQuality, fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + "/hNegPiHoughQuality", fNegPiHoughQuality, fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + "/hPosMuHoughQuality", fPosMuHoughQuality, fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + "/ch0/hdDecayedPionMuonXY", fDecayedPionXYch0[0] - fPosMuXY[0][0],
     fDecayedPionXYch0[1] - fPosMuXY[0][1], fWeight*fdGTK_w);
    Bool_t ch0ch1cond = (abs(fDecayedPionXYch0[0] - fPosMuXY[0][0]) < 20) &&
      (abs(fDecayedPionXYch0[1] - fPosMuXY[0][1]) < 20) && (abs(fDecayedPionXYch1[0] - fPosMuXY[1][0]) < 50)
      && (abs(fDecayedPionXYch1[1] - fPosMuXY[1][1]) < 50);
    if(ch0ch1cond)
      FillHisto(fPIDSwap + mask + cut + "/ch0/hdDecayedPionMuonXYcond", fDecayedPionXYch0[0] - fPosMuXY[0][0],
       fDecayedPionXYch0[1] - fPosMuXY[0][1], fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + "/ch0/hPosPiXY", fPosPiXY[0][0], fPosPiXY[0][1], fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + "/ch0/hNegPiXY", fNegPiXY[0][0], fNegPiXY[0][1], fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + "/ch0/hPosMuXY", fPosMuXY[0][0], fPosMuXY[0][1], fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + "/ch1/hdDecayedPionMuonXY", fDecayedPionXYch1[0] - fPosMuXY[1][0],
     fDecayedPionXYch1[1] - fPosMuXY[1][1], fWeight*fdGTK_w);
    if(ch0ch1cond)
      FillHisto(fPIDSwap + mask + cut + "/ch1/hdDecayedPionMuonXYcond", fDecayedPionXYch1[0] - fPosMuXY[1][0],
       fDecayedPionXYch1[1] - fPosMuXY[1][1], fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + "/ch1/hPosPiXY", fPosPiXY[1][0], fPosPiXY[1][1], fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + "/ch1/hNegPiXY", fNegPiXY[1][0], fNegPiXY[1][1], fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + "/ch1/hPosMuXY", fPosMuXY[1][0], fPosMuXY[1][1], fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + "/ch2/hPosPiXY", fPosPiXY[2][0], fPosPiXY[2][1], fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + "/ch2/hNegPiXY", fNegPiXY[2][0], fNegPiXY[2][1], fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + "/ch2/hPosMuXY", fPosMuXY[2][0], fPosMuXY[2][1], fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + "/ch3/hPosPiXY", fPosPiXY[3][0], fPosPiXY[3][1], fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + "/ch3/hNegPiXY", fNegPiXY[3][0], fNegPiXY[3][1], fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + "/ch3/hPosMuXY", fPosMuXY[3][0], fPosMuXY[3][1], fWeight*fdGTK_w);
    FillHisto(fPIDSwap + mask + cut + "/hSlopeDiff", fPosMudSlope, fWeight*fdGTK_w);
  }
}

void Kmu4StrictSelection::CutApply(TString mask) {
  Double_t m3PiK = ((fPosPionP + fNegPionP + fPosMuonP_fmID).M() - MKCH) / 1e3;
  
  std::vector<Bool_t> cutconditions{abs(fGTKMomt.Mag() - fBeamThreeMomt.Mag()) / 1.e3 < fCutdGTKMomt,
    (pow(m3PiK, 2)/pow(fCutEllipseA, 2) + pow(fvpt - fCutEllipseY, 2)/pow(fCutEllipseB, 2)) > 1,
    (fPosMuTotalQuality < fCutPosMuTotalQuality) && (fPosPiTotalQuality < fCutPosMuTotalQuality)
    && (fNegPiTotalQuality < fCutPosMuTotalQuality),
    fcost < fCutCost,
    fMMissSq_fmID < fCutMM2PiPiMu_misID,
    fMMissSq_2pi > fCutRSmu,
    fMMissSq_2pi > fCutL1Smu && fMMissSq_2pi < fCutL2Smu,
    fMMissSq_2pi > fCutRSmu || (fMMissSq_2pi > fCutL1Smu && fMMissSq_2pi < fCutL2Smu),
    false};
  // !((fNumomt < 8.8) && (fvpt < 0.034))
  //     fabs(fMMissSq_2pi - MPI) > 0.0034,

  Int_t noffalse = 0;
  Int_t falseid = -1;

  // false conditions from dGTK to PiPiMuMisID
  for(UInt_t i = 0; i < cutconditions.size() - 3; i++) {
    if(!cutconditions[i]) {
      falseid = i;
      noffalse++;
    }
  }

  // no false condition of upper one => fill histograms for all those conditions
  if(noffalse == 0) {
    for(UInt_t i = 0; i < fcut[1].size() - 2; i++) {
      TwoTracksVtxHisto(mask, fcut[1][i]);
      FillHistos(mask, fcut[1][i]);
      TrueFillHistos(mask, fcut[1][i]);
    }
  }
       
  if(noffalse == 1) {
    TwoTracksVtxHisto(mask, fcut[1][falseid + 1]);
    FillHistos(mask, fcut[1][falseid + 1]);
    TrueFillHistos(mask, fcut[1][falseid + 1]);
  }
  
  // Cuts After*
  TwoTracksVtxHisto(mask, fcut[0][0]);
  FillHistos(mask, fcut[0][0]);
  TrueFillHistos(mask, fcut[0][0]);

  for(UInt_t i = 1; i < fcut[0].size() - 3; i++) {
    if(!cutconditions[i - 1])
      return;
    TwoTracksVtxHisto(mask, fcut[0][i]);
    FillHistos(mask, fcut[0][i]);
    TrueFillHistos(mask, fcut[0][i]);
  }
  
  for(UInt_t i = 6; i < fcut[0].size(); i++) {
    if(cutconditions[i - 1]) {
      TwoTracksVtxHisto(mask, fcut[0][i]);
      FillHistos(mask, fcut[0][i]);
      TrueFillHistos(mask, fcut[0][i]);
    }
  }
}
